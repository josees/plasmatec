import sys
import os
import numpy as np
from scipy import special
import pandas as pd

"""
Implementación del método para el cálculo de la transformada rotacional
mencionado en el artículo 'Calculating  Rotational  Transform  Following  Field  Lines':
http://jasosx.ils.uec.ac.jp/JSPF/JSPF_TEXT/jspf2003/jspf2003_04/jspf2003_04_321.pdf
"""

def Leer_Datos(archivo):
    """
    Leer datos del archivo salida del BS-Solctra
    """
    datos = pd.read_csv(archivo, delimiter = '\t', names=['coord_x', 'coord_y', 'coord_z', 'mag_B'], skiprows=1, skipfooter=1, engine='python')
    # datos = np.loadtxt(archivo, delimiter = '\t', skiprows=1)
    # print('Datos leídos de "{}"'.format(archivo))
    # Usar pandas aquí más rápido y robusto pero es más complicado manejar los datos
    # Es posible que np.loadtxt() sea una opción viable en este caso
    return datos


def Derivad_num(vec_pos, vec_B, eje_magnetico_r = 0.2477, eje_magnetico_z = 0.0):
    """
    Cálculo de las derivadas respecto a phi. Los vectores de entrada deben estar en coordenadas cilíndricas.
    """
    # vec_pos = [r, phi, z]
    # vec_B = [b_r, b_phi, b_z]

    # Ecuaciones (3) en el artículo
    dr_dphi = (vec_pos[0]*vec_B[0])/vec_B[1]
    dr_A_dphi = (eje_magnetico_r*vec_B[0])/vec_B[1]
    dz_dphi = (vec_pos[0]*vec_B[2])/vec_B[1]
    dz_A_dphi = (eje_magnetico_r*vec_B[2])/vec_B[1]
    # print('eje magnético r: {0}, eje magnético z: {1}'.format(eje_magnetico_r, eje_magnetico_z))

    # Ecuación (5)
    denominador = 1./((vec_pos[0] - eje_magnetico_r)**2 + (vec_pos[2] - eje_magnetico_z)**2)
    dvartheta_dphi = ((vec_pos[0] - eje_magnetico_r)*denominador*(dz_dphi - dz_A_dphi)) - ((vec_pos[2] - eje_magnetico_z)*denominador*(dr_dphi - dr_A_dphi))
    # print('dvartheta_dphi = {0}'.format(dvartheta_dphi))

    return dvartheta_dphi


def sol_analitica(dvartheta_dphi, phi_max, alpha = 3.):
    """
    Solución analítica a la EDO (14)
    """
    # alpha = 3.
    k = (alpha/phi_max)**2
    w1 = phi_max*dvartheta_dphi*0.5*special.erf(k*phi_max)
    w2 = phi_max*dvartheta_dphi*0.5*special.erf(-k*phi_max)

    return (w1 - w2)/phi_max


def Funcion_Exp(dvartheta_dphi, phi_max, phi, alpha = 3.):
    """
    EDO (14)
    """
    # Quizás el valor alpha = 3 sugerido en el artículo no es el valor adecuado para nuestros cálculos
    K = (alpha/phi_max)**2

    return dvartheta_dphi*(alpha/np.sqrt(np.pi))*np.exp(-K*(phi**2))


def RK4(paso, i_cond, dvartheta_dphi, phi_max):
    """
    Implementación de método numérico Runge-Kutta 4 para resolver la EDO (14)
    """

    k1 = paso*Funcion_Exp(dvartheta_dphi, phi_max, i_cond)
    k2 = paso*Funcion_Exp(dvartheta_dphi, phi_max, i_cond + (k1/2))
    k3 = paso*Funcion_Exp(dvartheta_dphi, phi_max, i_cond + (k2/2))
    k4 = paso*Funcion_Exp(dvartheta_dphi, phi_max, i_cond + k3)

    return i_cond + (1./6)*(k1 + 2*k2 + 2*k3 + k4)


def Cartesianas_a_Cilindricas_Puntos(vector, nRot):
    """
    Transformación de coord cartesianas a polares: (x, y, z) -> (r, phi ,z)
    array en cartesianas -> arry en cilindricas
    """
    vector_cil = np.array(vector)
    r = np.sqrt(vector[0]**2 + vector[1]**2)
    # cambiar a arccos o arcsen para evitar la division por cero
    phi = np.arctan(vector[1]/vector[0])

    if vector[0] > 0 and vector[1] > 0:
        phi = 2*np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el primer cuadrante {0:.2f}°'.format(phi*180/np.pi))
    elif vector[0] < 0 and vector[1] > 0:
        phi = 2*np.pi + np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el segundo cuadrante {0:.2f}°'.format(phi*180/np.pi))
    elif vector[0] < 0 and vector[1] < 0:
        phi = 2*np.pi + np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el tercer cuadrante {0:.2f}°'.format(phi*180/np.pi))
    else:
        phi = 2*np.pi + 2*np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el cuarto cuadrante {0:.2f}°'.format(phi*180/np.pi))

    vector_cil[0] = r
    vector_cil[1] = phi
    vector_cil[2] = vector[2]
    # print('vector en cilíndricas: {}'.format(vector))

    return vector_cil


def Cartesianas_a_Cilindricas_Campo(posA, posB, pos_vec_B, mag_B):
    """
    Obtiene el vector campo magnético construyéndolo a partir de dos vectores posición
    (en coordenadas cartesianas) y la magnitub de B. Retorna el vector campo magnético
    en coordenadas cilíndricas
    """
    # print('Vectores iniciales: vectA {}, vectB {}'.format(posA, posB))
    # Se calcula el vector que va del punto A a B
    vect = posB - posA
    # print('Vector diferencia: {}'.format(vect))

    # norma del vector cartesiano
    norma = np.linalg.norm(vect)

    # print('norma vector diferencia: {0:.2f}'.format(norma))

    # Se calcula el vector B en coordenadas cartesianas
    vect_B = vect*mag_B/norma
    # print('Vector B en cartesianas: {}'.format(vect_B))

    # Se construye la matriz de transformación a coordenadas cilíndricas.
    # Se toma como punto de referencia para el ángulo theta un punto arbitrario pos_vec_B.
    # Este punto será la posición A, es decir, el primer punto de la trayectoria considerado para coonstruir B.

    # componente r del la posicón de B en cilíndricas
    # r = np.sqrt((pos_vec_B[0]**2) + (pos_vec_B[1]**2))

    cos_theta = np.cos(pos_vec_B[1])  # pos_vec_B[0]/r
    sen_theta = np.sin(pos_vec_B[1])  # pos_vec_B[1]/r
    # print('componente r posición B: {0:.2f}, ángulo: {1}'.format(r, pos_vec_B[1]*180/np.pi))

    mat_tr = np.array([[cos_theta,     sen_theta,  0.0],
                       [-sen_theta,    cos_theta,  0.0],
                       [0.0,           0.0,        1.0]])

    # print('matriz de transformación: \n{}'.format(mat_tr))
    # Se transforma el vector a coord cilíndricas usando la matriz de transformación

    # vect_B_cil = np.zeros(3, dtype=np.float64)
    # vect_B_cil[0] = (mat_tr[0, 0]*vect_B[0]) + (mat_tr[0, 1]*vect_B[1])
    # a = (mat_tr[1, 0] * vect_B[0])
    # b = (mat_tr[1, 1] * vect_B[1])
    # vect_B_cil[1] = a + b
    # vect_B_cil[2] = vect_B[2]

    vect_B_cil = mat_tr @ vect_B

    # print('Vector B en cilíndricas: {}'.format(vect_B_cil))

    return vect_B_cil


def Cartesianas_a_Cilindricas(vector, nRot):
    """
    Transformación de coord cartesianas a polares: (x, y, z) -> (r, phi ,z)
    """
    r = np.sqrt(vector[0]**2 + vector[1]**2)
    phi = np.arctan(vector[1]/vector[0])  # cambiar a arccos o arcsen para evitar la division por cero

    if vector[0] > 0 and vector[1] > 0:
        phi = 2*np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el primer cuadrante {0:.2f}°'.format(phi*180/np.pi))
    elif vector[0] < 0 and vector[1] > 0:
        phi = 2*np.pi + np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el segundo cuadrante {0:.2f}°'.format(phi*180/np.pi))
    elif vector[0] < 0 and vector[1] < 0:
        phi = 2*np.pi + np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el tercer cuadrante {0:.2f}°'.format(phi*180/np.pi))
    else:
        phi = 2*np.pi + 2*np.pi + phi + 2*np.pi*(nRot - 1)
        # print('punto en el cuarto cuadrante {0:.2f}°'.format(phi*180/np.pi))

    # vector[0] = r
    # vector[1] = phi
    # print('vector en cilíndricas: {}'.format(vector))

    # return vector
    return  r, phi, vector[2]

def Componentes_Campo(puntoA, puntoB, mag_B):
    """
    Retorna el vector campo magnético
    """
    # Se calcula el vector que va del punto A a B
    vect = puntoB - puntoA
    norma = np.linalg.norm(vect)

    # Se calcula calcula el vector B y se retorna
    return vect*mag_B/norma


def Calculo_iota(archivo, phi_0 = 0., phi_max = 90.):
    """
    Función que calcula la transformada rotacional en el ángulo 'phi_0', dados los datos en 'archivo'
    """
    datos = Leer_Datos(archivo)         # Se lee el archivo con los datos de la trayectoria
    nPasos, _ = datos.shape             # Se obtiene el número de puntos de trayectoria calculados
    i_cond = 0                          # Condición inicial w(0) = 0
    pasoRK = 0.001                      # paso usando en el RK4 para calcular los puntos de trayectoria
    nRot = 0                            # número de rotaciones
    phi_0 = phi_0*np.pi/180             # phi_0 de grados a radianes
    phi_max = phi_max*np.pi/180         # phi_max de grados a radianes

    print('Cálculo de iota con el archivo {0}, phi = {1}°, phi_max = {2}°'.format(archivo, phi_0*180/np.pi, phi_max*180/np.pi))

    if phi_0 < 2*np.pi:                 # Se comprueba que el ángulo sea mayor que 2pi
        phi_0 = phi_0 + 2*np.pi
        # print('El ángulo inicial debe ser mayor que 2pi.\nÁngulo inicial ajustado a {0:.3f}pi approx {1:.3f}°'.format(phi_0/np.pi, phi_0*180/np.pi))

    phi_i = phi_0 - phi_max             # Límite inferior para la integración
    phi_f = phi_0 + phi_max             # Límite superior para la integración
    w_i = 0.                            # Valor inicial para w_i
    w_f = 0.                            # Valor inicial para w_f

    for iPaso in range(1, nPasos):  # se comienza a iterar desde el segundo punto
        # Se toman dos puntos para calcular el vector campo magnético
        # print('Iteración {}:'.format(iPaso))
        puntoA = np.array([datos['coord_x'][iPaso - 1], datos['coord_y'][iPaso - 1], datos['coord_z'][iPaso - 1]])
        puntoB = np.array([datos['coord_x'][iPaso], datos['coord_y'][iPaso], datos['coord_z'][iPaso]])

        # Se obtienen las coordenadas de los puntos de la trayectoria en coord cilíndricas
        puntoB = Cartesianas_a_Cilindricas(puntoB, nRot)
        puntoA = Cartesianas_a_Cilindricas(puntoA, nRot)
        # print('pA: {}, pB: {}'.format(puntoA[1]*180/np.pi, puntoB[1]*180/np.pi))

        # Se evalúa la componente toroidal para determinar el número de rotaciones dadas
        if abs(puntoA[1] - puntoB[1]) > np.pi:
            # print('B_phi = A: {0:.3f}°, B: {1:.3f}°. Diff: {2:.3f}°'.format(puntoA[1]*180/np.pi, puntoB[1]*180/np.pi, abs(puntoA[1] - puntoB[1])*180/np.pi))
            nRot += 1
            puntoB = np.array([datos['coord_x'][iPaso], datos['coord_y'][iPaso], datos['coord_z'][iPaso]])
            puntoB = Cartesianas_a_Cilindricas(puntoB, nRot)
            # print('nuevo B_phi = B: {0:.2f}°'.format(puntoB[1]*180/np.pi))

        # Se obtiene el vector campo magnético
        campo = Componentes_Campo(puntoA, puntoB, datos['mag_B'][iPaso])
        # print('\tVector campo magnético en cartesianas: {}'.format(campo))

        # Se obtienen las componentes de B en coord cilíndrias con 0 < B_phi < 2pi
        campo = Cartesianas_a_Cilindricas(campo, 0)

        # print('\tCoordenada toroidal: {}'.format(puntoB[1]*180/np.pi))

        # Se obtiene la constante dvartheta_dphi
        dvartheta_dphi = Derivad_num(puntoB, campo)

        # Se calcula el valor w(phi)
        i_cond = RK4(pasoRK, i_cond, dvartheta_dphi, phi_max)
        # print('\tw(i = {}) = {}'.format(iPaso, i_cond))

        # Evaluación de w_i y w_f
        if puntoB[1] > phi_i and w_i == 0.:  # esta condición es peligrosa, mejor una diferencia entre valores
            w_i = i_cond
            print('obtenido w_i = {:.3f}'.format(w_i))
        elif abs(puntoB[1] - phi_0) < 0.01:
            campo_0 = np.linalg.norm(campo)
        elif puntoB[1] > phi_f and w_f == 0.:
            w_f = i_cond
            print('obtenido w_f = {:.3f}'.format(w_f))
            break
    # fin del ciclo que itera sobre los puntos de trayectoria

    # Se calcula la tranformada rotacional
    iota = (w_f - w_i)/phi_max

    # print('Transformada rotacional para ángulo {0:.1f}°: iota = {1:.5f}'.format(phi_0*180/np.pi, iota))
    # Se retorna el coord_r, iota y |B|
    return puntoB[0], iota, np.linalg.norm(campo)


def Generar_datos_r_iota(ruta):
    """
    Función que hace el cálculo de la transformada para cada archivo con datos de trayectoria
    """
    # ruta es el directorio donde están los archivos, se asume que los archivos y este script están en la mismo directorio, esto debe cambiarse más adelante

    archivos_trayectoria = os.listdir(ruta)     # se obtiene la lista de archivos de trayectoria
    nTrayectorias = len(archivos_trayectoria)   # se obtiene el número de archivos
    data = np.zeros([nTrayectorias, 3])         # array donde se van a almacenar los datos
    iCont = 0
    # print('{}'.format(archivos_trayectoria))
    for iArch in archivos_trayectoria:
        iArch = '{}/{}'.format(ruta, iArch)
        # stats = os.stat(iArch)
        # print('tamaño de {}: {}'.format(iArch, stats.st_size))
        # print('archivo actual: {}'.format(iArch))
        data[iCont, 0], data[iCont, 1], data[iCont, 2] = Calculo_iota(iArch)    # para cada archivo se calcula iota y se guarda en el array
        iCont += 1
    np.savetxt('datos_r_vs_iota.txt', data)     # finalmente se guardan los datos en un archivo de texto


def Calculo_iota_2(archivo, archivo_cil):
    """
    Función que calcula la transformada rotacional en el ángulo 'phi_0', dados los datos en 'archivo_cilindricas'
    Se olvida el phi_max y se itera usando todos los puntos de una trayectoria
    """
    datos = Leer_Datos(archivo)         # Se lee el archivo con los datos de la trayectoria
    nPasos, _ = datos.shape             # Se obtiene el número de puntos de trayectoria calculados
    i_cond = 0                          # Condición inicial w(0) = 0
    nRot = 0                            # número de rotaciones
    # w_f = 0.                          # Valor inicial para w_f
    datos_cil = np.loadtxt(archivo_cil, skiprows=1)  # Se lee el archivo con los datos de la trayectoria
    nPasos2, _ = datos_cil.shape
    datos_iota = np.zeros((nPasos, 2))

    # # Se obtiene la identificacion de los datos de trayectoria
    id = archivo.split('/')[1].split('.')[0]
    # print('ruta: {}, corte: {}'.format(archivo, id))

    phi_max = datos_cil[nPasos2 - 1][1]
    # print('phi_max = {:.3f}°'.format(phi_max*180/np.pi))

    for iPaso in range(1, nPasos):  # se comienza a iterar desde el segundo punto
        # Se toman dos puntos para calcular el vector campo magnético
        puntoA = np.array([datos['coord_x'][iPaso - 1], datos['coord_y']
                           [iPaso - 1], datos['coord_z'][iPaso - 1]])
        puntoB = np.array([datos['coord_x'][iPaso], datos['coord_y']
                           [iPaso], datos['coord_z'][iPaso]])

        # Se obtienen las conmponentes de los puntos en coord cilíndricas
        puntoA_cilindricas = np.zeros(3, dtype=np.float64)
        puntoB_cilindricas = np.zeros(3, dtype=np.float64)
        puntoA_cilindricas = Cartesianas_a_Cilindricas_Puntos(puntoA, nRot)
        puntoB_cilindricas = Cartesianas_a_Cilindricas_Puntos(puntoB, nRot)

        # Se evalúa la componente toroidal para determinar el número de rotaciones dadas
        if abs(puntoA_cilindricas[1] - puntoB_cilindricas[1]) > np.pi:
            nRot += 1
            puntoB_cilindricas = Cartesianas_a_Cilindricas_Puntos(puntoB, nRot)

        # Se calcula el campo B en coordenadas cilíndricas a partir de puntoA y puntoB
        vect_B = Cartesianas_a_Cilindricas_Campo(puntoA, puntoB, puntoA_cilindricas, datos['mag_B'][iPaso - 1])

        # Se obtiene la constante dvartheta_dphi
        dvartheta_dphi = Derivad_num(puntoA_cilindricas, vect_B)
        paso = puntoB_cilindricas[1] - puntoA_cilindricas[1]

        # Se calcula el valor w(phi)
        i_cond = RK4(paso, puntoA_cilindricas[1], i_cond, dvartheta_dphi, phi_max)

        # Se calcula la tranformada rotacional
        iota = i_cond/phi_max

        # Se guardan los datos
        datos_iota[iPaso - 1, 0] = iota
        datos_iota[iPaso - 1, 1] = puntoA_cilindricas[1]
    # fin del ciclo que itera sobre los puntos de trayectoria

    np.savetxt('evolucion_iota/datos_iota_vs_phi_{}.txt'.format(id), datos_iota)
        # print('radio: {0}, iota: {1}'.format(r_0, iota))
    # print('valor de iota para el paso {}: {:.5f}'.format(iPaso, iota))

    # Se retornan los valores finales de iota y phi_max
    return iota, phi_max


def Calculo_iota_3(punto):
    """
    Función que calcula la transformada rotacional para 'punto'.
    Iota se calcula usando la ecuación [2] del artículo.
    punto: vector en coordenadas cilíndricas, donde theta [0, +inf[
    """

    # Se establecen los valores del eje magnético
    r_eje_magnetico = 0.2477
    z_eje_magnetico = 0.0

    var_theta = np.arctan((punto[2] - z_eje_magnetico)/(punto[0] - r_eje_magnetico))

    iota = var_theta/punto[1]

    return iota, punto[1]


def Calculo_iota_4(archivo_trayectoria):
    """
    Este método calcula la diferencia angular entre puntos separados por una revolución. Luego
    se suman esas diferencias para cada revolución y se divide entre el número de revoluciones
    archivo_trayectoria: es un archivo que contiene los datos de una trayectoria en
    coordenadas toroidales
    """
    datos = np.loadtxt(archivo_trayectoria, skiprows = 1)

    nPasos, _ = datos.shape
    punto_inicial = np.zeros(3)
    punto_final = np.zeros(3)

    # Este contador indicará la revolución en la que se está
    nRev = 1

    # Variable a la que suman los delta_phi
    sumatoria_delta_phi = 0

    print('\n\n\nProcesando archivo {}'.format(archivo_trayectoria))

    for iPaso in range(nPasos):
        # si estamos en el primer paso se toma como punto inicial el primer punto
        if iPaso == 0:
            punto_inicial = datos[iPaso, :]


        elif datos[iPaso, 1] < -2*np.pi*nRev:
            # la condición original era para entrar a
            # este if era datos[iPaso, 1] > 2*np.pi*nRev
            # Se cambió por que las coordenadas toroidales
            # aumentan en el sentido negativo

            # print('Se completa la revolución {}'.format(nRev))
            # Se almacena el primer punto que completa una revolución
            punto_final = datos[iPaso, :]

            # print('\tpunto inicial: {}\n\tpunto final: {}'.format(punto_inicial, punto_final))

            # Se aumenta el contador
            nRev += 1

            # Se calcula la diferencia en los ángulos toroidales y se suma a la varible sumatoria
            delta_phi = abs(punto_final[2] - punto_inicial[2])
            sumatoria_delta_phi += delta_phi
            # print('\tdiferencia angular poloidal: {}\n'.format(delta_phi))

            # Se define al punto final como el nuevo inicial para continuar el ciclo
            punto_inicial = np.array(punto_final)

        # else:
        #     continue

    # Finalmente se divide la sumatoria de deltas entre el número de de rovoluciones
    # para obtener el valor de la transformada rotacional
    iota = sumatoria_delta_phi/(nRev*2*np.pi)

    print('\tsumatoria: {:.7f}\tnRev: {}\tiota: {:.7f}\tlineas: {}'.format(sumatoria_delta_phi, nRev, iota, nPasos))

    return iota, nPasos


def Generar_datos_convergencia_iota3(ruta_datos_cil):
    """
    Función que calcula la evolución de iota respecto al ángulo para una las trayectorias.
    """
    archivos = os.listdir(ruta_datos_cil)

    for iArch in archivos:
        datos = np.loadtxt('{}{}'.format(ruta_datos_cil, iArch), skiprows=1)
        print('Procesando archivo: {}'.format(iArch))
        nPasos, _  = datos.shape

        datos_iota = np.zeros((nPasos, 2))

        for iPaso in range(nPasos):
            iota, ang = Calculo_iota_3(datos[iPaso])
            datos_iota[iPaso, 0] = iota
            datos_iota[iPaso, 1] = ang

        np.savetxt('{}{}_datos_iota_vs_phi.txt'.format(ruta_datos_cil, iArch.split('_cilindricas')[0]), datos_iota)


def Generar_datos_convergencia_iota(ruta_datos_cart = 'results_49865.meta.cnca/', ruta_datos_cil = 'results_cilindricas_inf/'):
    """
    Función que calcula la evolución de iota respecto al ángulo para todas las trayectorias.
    """
    arch_trayectorias_cart = os.listdir(ruta_datos_cart)     # se obtiene la lista de archivos de trayectoria
    arch_trayectorias_cil = os.listdir(ruta_datos_cil)

    arch_trayectorias_cart.sort()
    arch_trayectorias_cil.sort()

    # Prueba para comprobar que las listas tienen la misma longitud
    assert len(arch_trayectorias_cil) == len(arch_trayectorias_cart),"Las listas de archivos no tienen la misma cantidad de elementos"

    datos_iota = np.zeros((len(arch_trayectorias_cil), 2))

    for jCont in range(len(arch_trayectorias_cil)):
        datos_iota[jCont, 0], datos_iota[jCont, 1] = Calculo_iota_2(arch_trayectorias_cart[jCont], arch_trayectorias_cil[jCont])

    np.savetxt('datos_iota_vs_phi.txt', datos_iota)


if __name__ == '__main__':
    archivo = sys.argv[1]
    phi_0 = sys.argv[2]
    phi_max = sys.argv[3]
    Calculo_iota(archivo, phi_0, phi_max)
