function B = calculate_B( l, R )
% calculates separately B field components



%     Copyright (C) 2007 
%
%       mag. David Erzen
%       Faculty of Mechanical Engineering
%       LECAD Laboratory
%       Askerceva 6
%       1000 Ljubljana
%       SLOVENIA
%       contact email: david.erzen@lecad.uni-lj.si
%       
%       Prof. John P. Verboncoeur
%       Plasma Theory and Simulation Group
%       University of California
%       Berkeley, CA 94720-1730 USA
%       
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
    



    global_variables;
% TODO: use the correct function
    % calculates B field vector in point R
    B = zeros( 3, 1);
    
        Bx = eval( field_BfunctionX );
        By = eval( field_BfunctionY );
        Bz = eval( field_BfunctionZ );    
      
    % time factor multiplication
     if absolute_time >= 0   % time factor multiplication WHEN SIMULATING        
        t = absolute_time;
        B(1) = Bx*eval(field_BTimefunctionX);
        B(2) = By*eval(field_BTimefunctionY);
        B(3) = Bz*eval(field_BTimefunctionZ);
     else                   % if time set to negative value means no time factor needed
        B(1) = Bx;
        B(2) = By;
        B(3) = Bz;
    end
end