function dR = acceleration( l, R )
% calcualtes acceleration (F/m), first three lines are used for ODE solving



%     Copyright (C) 2007 
%
%       mag. David Erzen
%       Faculty of Mechanical Engineering
%       LECAD Laboratory
%       Askerceva 6
%       1000 Ljubljana
%       SLOVENIA
%       contact email: david.erzen@lecad.uni-lj.si
%       
%       Prof. John P. Verboncoeur
%       Plasma Theory and Simulation Group
%       University of California
%       Berkeley, CA 94720-1730 USA
%       
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
    


    global_variables;
    
    t = absolute_time;
        
    dR = zeros( 6, 1 );
    dR(1) = R(4);
    dR(2) = R(5);
    dR(3) = R(6);
    B = calculate_B( l, R(1:3) );
    E = calculate_E( l, R(1:3) );
    tempEt = eval(field_ETimefunctionX);
    dR(4) = particle_charge/particle_mass*( E(1)+ R(5)*B(3)-R(6)*B(2));
    dR(5) = particle_charge/particle_mass*( E(2)+ R(6)*B(1)-R(4)*B(3));
    dR(6) = particle_charge/particle_mass*( E(3)+ R(4)*B(2)-R(5)*B(1))+ eval(field_GfunctionZ);
end