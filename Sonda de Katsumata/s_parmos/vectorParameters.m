function [ vector_rotation, vector_conePos, vector_pos, vector_height] = vectorParameters( particle_pos, vector )
% calcualtes vector parameters for the force and velocity vectors
% the parameters are vector length, heigth, position, tip position....


%     Copyright (C) 2007 
%
%       mag. David Erzen
%       Faculty of Mechanical Engineering
%       LECAD Laboratory
%       Askerceva 6
%       1000 Ljubljana
%       SLOVENIA
%       contact email: david.erzen@lecad.uni-lj.si
%       
%       Prof. John P. Verboncoeur
%       Plasma Theory and Simulation Group
%       University of California
%       Berkeley, CA 94720-1730 USA
%       
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    
    
    if norm( vector ) > 0
        vector_normalised = vector/norm( vector );
    else
        vector_normalised = vector;
    end;
    
    rotation_angle = acos( dot( vector_normalised, [ 0; 1; 0 ] ) );
    rotation_axis = cross( [ 0 1 0 ], vector_normalised );
    vector_rotation = cat( 2, rotation_axis, rotation_angle );
    
    vector_conePos = particle_pos + vector;
    vector_pos = particle_pos + vector/2;
    vector_height = norm( vector );
    
end