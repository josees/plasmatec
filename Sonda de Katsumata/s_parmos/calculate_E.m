function E = calculate_E( l, R )
% calculates separately E field components



%     Copyright (C) 2007 
%
%       mag. David Erzen
%       Faculty of Mechanical Engineering
%       LECAD Laboratory
%       Askerceva 6
%       1000 Ljubljana
%       SLOVENIA
%       contact email: david.erzen@lecad.uni-lj.si
%       
%       Prof. John P. Verboncoeur
%       Plasma Theory and Simulation Group
%       University of California
%       Berkeley, CA 94720-1730 USA
%       
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
    



    global_variables;
% TODO: use the correct function
    % calculates E field vector in point R 
    E = zeros( 3, 1);
    
        Ex = eval( field_EfunctionX );
        Ey = eval( field_EfunctionY );
        Ez = eval( field_EfunctionZ );    
    
    if absolute_time >= 0   % time factor multiplication WHEN SIMULATING        
        t = absolute_time;
        E(1) = Ex*eval(field_ETimefunctionX);
        E(2) = Ey*eval(field_ETimefunctionY);
        E(3) = Ez*eval(field_ETimefunctionZ);    
    else                    % if time set to negative value means no time factor needed
        E(1) = Ex;
        E(2) = Ey;
        E(3) = Ez;
    end
end