function prepare_information( handles )
% prepares information about the simulation parameters, like particle
% initial position, velocity, mass, charge, electric and magnetic field
% function...



%     Copyright (C) 2007 
%
%       mag. David Erzen
%       Faculty of Mechanical Engineering
%       LECAD Laboratory
%       Askerceva 6
%       1000 Ljubljana
%       SLOVENIA
%       contact email: david.erzen@lecad.uni-lj.si
%       
%       Prof. John P. Verboncoeur
%       Plasma Theory and Simulation Group
%       University of California
%       Berkeley, CA 94720-1730 USA
%       
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
    


    global_variables;
    
    % set particle information
    particle_initPos = (str2num( get( handles.edtInitPos, 'String' ) ))';
    particle_initVel = (str2num( get( handles.edtInitVel, 'String' ) ))';
    particle_mass = str2num( get( handles.edtMass, 'String' ) );
    particle_charge = str2num( get( handles.edtCharge, 'String' ) );
    
    % set E field information function as a string
    switch get( handles.popElecField,'Value' )
        
        case EFIELD_NONE                                        % no electric field is selected            
            field_Efunction = '[ 0, 0, 0 ]';
            field_EfunctionX = '0';
            field_EfunctionY = '0';
            field_EfunctionZ = '0';
            field_EfunctionOK = 1;
            field_Echosen = EFIELD_NONE;
            
        case EFIELD_CONSTANT                                    % constant electric filed in x direction            
            parameters = get( handles.edtElecField,'String' );
            field_Efunction = strcat( '[', parameters, ', 0, 0 ]' );
            field_EfunctionX = parameters;
            field_EfunctionY = '0';
            field_EfunctionZ = '0';
            field_EfunctionOK = 1;
            field_Echosen = EFIELD_CONSTANT;
            
        case EFIELD_CUSTOM                                      % a user defined electric field is selected            
            field_Efunction = get( handles.edtElecField,'String' );
            [field_EfunctionX, remain] = strtok( field_Efunction, {',', '[', ']'} );
            [field_EfunctionY, remain] = strtok( remain, {',', '[', ']'} );
            [field_EfunctionZ, remain] = strtok( remain, {',', '[', ']'} );
            field_EfunctionOK = 1;
            field_Echosen = EFIELD_CUSTOM;
    end
    
    % set E field time factor
    switch get(handles.popTimeElecField, 'Value')
        case EFIELD_TIMECONSTANT                                    % constant electric field
            field_ETimefunctionX = '1';
            field_ETimefunctionY = '1';
            field_ETimefunctionZ = '1';
        case EFIELD_SINUSOIDAL                                      % sinusoidal electric field
            parameters = str2num( get( handles.edtTimeElecField,'String' ));
            field_ETimefunctionX = strcat('sin(',num2str(parameters(1)), '*t+', num2str(parameters(2)), ')');
            field_ETimefunctionY = strcat('sin(',num2str(parameters(1)), '*t+', num2str(parameters(2)), ')');
            field_ETimefunctionZ = strcat('sin(',num2str(parameters(1)), '*t+', num2str(parameters(2)), ')');
        case EFIELD_TIMECUSTOM                                      % custom time factor
            field_ETimefunction = get( handles.edtTimeElecField,'String' );
            [field_ETimefunctionX, remain] = strtok( field_ETimefunction, {',', '[', ']'} );
            [field_ETimefunctionY, remain] = strtok( remain, {',', '[', ']'} );
            [field_ETimefunctionZ, remain] = strtok( remain, {',', '[', ']'} );
    end
    
    % set B field information function as a string
    switch get( handles.popMagField,'Value' )
        case BFIELD_NONE                                        % no magnetic field is selected
            field_Bfunction= '[ 0, 0, 0 ]';
            field_BfunctionX= '0';
            field_BfunctionY= '0';
            field_BfunctionZ= '0';
            field_BfunctionOK = 1;
            field_Bchosen = BFIELD_NONE;
            
        case BFIELD_CONSTANT                                    % constant magnetic filed in z direction
            parameters = str2num( get( handles.edtMagField,'String' ));            
            field_Bfunction = strcat( '[ 0, 0,', num2str( parameters ), ']' );
            field_BfunctionX = '0';
            field_BfunctionY = '0,';
            field_BfunctionZ = num2str( parameters );
            field_BfunctionOK = 1;
            field_Bchosen = BFIELD_CONSTANT;
            
        case BFIELD_EXPONENT                                    % magnetic field in z direction, exponential fall in x direction
            parameters = str2num( get( handles.edtMagField,'String' ) );
            field_Bfunction = strcat( '[ 0, 0,', num2str( parameters(1) ), '*exp(-R(1)/', num2str( parameters(2) ), ')]' );        
            field_BfunctionX = '0';
            field_BfunctionY = '0';
            field_BfunctionZ = strcat( num2str( parameters(1) ), '*exp(-R(1)/', num2str( parameters(2) ), ')' );        
            field_BfunctionOK = 1;
            field_Bchosen = BFIELD_EXPONENT;
            
        case BFIELD_TORUS                                       % magnetic field in a torus
            parameters = str2num( get( handles.edtMagField,'String' ) );
            field_Bfunction = strcat('(', num2str( parameters(1) ), '*', num2str( parameters(2) ), '/(R(1)^2+R(2)^2+1e-6))*[ -R(2), R(1), 0 ]' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionX = strcat('(', num2str( parameters(1) ), '*', num2str( parameters(2) ), '/(R(1)^2+R(2)^2+1e-6))*( -R(2) )' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionY = strcat('(', num2str( parameters(1) ), '*', num2str( parameters(2) ), '/(R(1)^2+R(2)^2+1e-6))*( R(1) )' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionZ = strcat('(', num2str( parameters(1) ), '*', num2str( parameters(2) ), '/(R(1)^2+R(2)^2+1e-6))* 0 ' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionOK = 1;
            field_Bchosen = BFIELD_TORUS;
            
        case BFIELD_TORUS_POLOIDAL                              % magnetic field in a torus with poloidal field
            parameters = str2num( get( handles.edtMagField,'String' ) );
            field_Bfunction = strcat( '(',num2str(parameters(1)), ')*', num2str(parameters(2)),'/(R(1)^2 + R(2)^2 + 1e-6 )*','[ -R(2), R(1),0 ]','+', num2str(parameters(3)),'*[ R(3)*R(1)/sqrt(R(1)^2+R(2)^2+1e-6), R(3)*R(2)/sqrt(R(1)^2+R(2)^2+1e-6),', num2str(parameters(2)),'-sqrt(R(1)^2+R(2)^2+1e-6) ]*sqrt(R(1)^2+R(2)^2+R(3)^2+', num2str(parameters(2)), '^2-2*', num2str(parameters(2)), '*sqrt(R(1)^2+R(2)^2))');
            field_BfunctionX = strcat( '(',num2str(parameters(1)), ')*', num2str(parameters(2)),'/(R(1)^2 + R(2)^2 + 1e-6 )*','( -R(2) )','+', num2str(parameters(3)),'*( R(3)*R(1)/sqrt(R(1)^2+R(2)^2+1e-6) )*sqrt(R(1)^2+R(2)^2+R(3)^2+', num2str(parameters(2)), '^2-2*', num2str(parameters(2)), '*sqrt(R(1)^2+R(2)^2))');
            field_BfunctionY = strcat( '(',num2str(parameters(1)), ')*', num2str(parameters(2)),'/(R(1)^2 + R(2)^2 + 1e-6 )*','(R(1))','+', num2str(parameters(3)),'*( R(3)*R(2)/sqrt(R(1)^2+R(2)^2+1e-6))*sqrt(R(1)^2+R(2)^2+R(3)^2+', num2str(parameters(2)), '^2-2*', num2str(parameters(2)), '*sqrt(R(1)^2+R(2)^2))');
            field_BfunctionZ = strcat(  num2str(parameters(3)),'*(' , num2str(parameters(2)),'-sqrt(R(1)^2+R(2)^2+1e-6) )*sqrt(R(1)^2+R(2)^2+R(3)^2+', num2str(parameters(2)), '^2-2*', num2str(parameters(2)), '*sqrt(R(1)^2+R(2)^2))');
            field_BfunctionOK = 1;
            field_Bchosen = BFIELD_TORUS_POLOIDAL;
            
        case BFIELD_CONCENTRIC                                       % magnetic field in a concentric geometry
            parameters = str2num( get( handles.edtMagField,'String' ) );
            field_Bfunction = strcat('(', num2str( parameters(1) ), '*', num2str( parameters(2) ), '/(R(1)^2+R(2)^2+1e-6))*[ -R(2), R(1), 0 ]' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionX = strcat('(', num2str( parameters(1) ), '*', num2str( parameters(2) ), '/(R(1)^2+R(2)^2+1e-6))*( -R(2) )' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionY = strcat('(', num2str( parameters(1) ), '*', num2str( parameters(2) ), '/(R(1)^2+R(2)^2+1e-6))*( R(1) )' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionZ = strcat('(', num2str( parameters(1) ), '*', num2str( parameters(2) ), '/(R(1)^2+R(2)^2+1e-6))* 0 ' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionOK = 1;
            field_Bchosen = BFIELD_CONCENTRIC;
            
        case BFIELD_CUSTOM                                      % a user defined magnetic field is selected                        
            field_Bfunction = get( handles.edtMagField,'String' );
            [field_BfunctionX, remain] = strtok( field_Bfunction, {',', '[', ']'} );
            [field_BfunctionY, remain] = strtok( remain, {',', '[', ']'} );
            [field_BfunctionZ, remain] = strtok( remain, {',', '[', ']'} );
            field_BfunctionOK = 1;
            field_Bchosen = BFIELD_CUSTOM;
                    
        case BFIELD_MIRROR
            parameters = str2num( get( handles.edtMagField,'String' ) );
            
            %[parameters(3)*(R(1))/((R(1))^2+(R(2))^2+(R(3)+parameters(1)/2)^2)^(3/2) + parameters(2)*(R(1))/((R(1))^2+(R(2))^2+(R(3)-parameters(1)/2)^2)^(3/2),
            %parameters(3)*(R(2))/((R(1))^2+(R(2))^2+(R(3)+parameters(1)/2)^2)^(3/2) + parameters(2)*(R(2))/((R(1))^2+(R(2))^2+(R(3)-parameters(1)/2)^2)^(3/2),
            %parameters(3)*(R(3)+parameters(1)/2)/((R(1))^2+(R(2))^2+(R(3)+parameters(1)/2)^2)^(3/2) + parameters(2)*(R(3)-parameters(1)/2)/((R(1))^2+(R(2))^2+(R(3)-parameters(1)/2)^2)^(3/2)] 

            
            field_Bfunction = strcat('[',num2str(parameters(3)),'*(R(1))/((R(1))^2+(R(2))^2+(R(3)+',num2str(parameters(1)),'/2)^2)^(3/2) + ',num2str(parameters(2)),'*(R(1))/((R(1))^2+(R(2))^2+(R(3)-',num2str(parameters(1)),'/2)^2)^(3/2),', num2str(parameters(3)),'*(R(2))/((R(1))^2+(R(2))^2+(R(3)+',num2str(parameters(1)),'/2)^2)^(3/2) + ',num2str(parameters(2)),'*(R(2))/((R(1))^2+(R(2))^2+(R(3)-',num2str(parameters(1)),'/2)^2)^(3/2),',num2str(parameters(3)),'*(R(3)+',num2str(parameters(1)),'/2)/((R(1))^2+(R(2))^2+(R(3)+',num2str(parameters(1)),'/2)^2)^(3/2) + ',num2str(parameters(2)),'*(R(3)-',num2str(parameters(1)),'/2)/((R(1))^2+(R(2))^2+(R(3)-',num2str(parameters(1)),'/2)^2)^(3/2)]'); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionX = strcat( num2str(parameters(3)),'*(R(1))/((R(1))^2+(R(2))^2+(R(3)+',num2str(parameters(1)),'/2)^2)^(3/2) + ',num2str(parameters(2)),'*(R(1))/((R(1))^2+(R(2))^2+(R(3)-',num2str(parameters(1)),'/2)^2)^(3/2)' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionY = strcat( num2str(parameters(3)),'*(R(2))/((R(1))^2+(R(2))^2+(R(3)+',num2str(parameters(1)),'/2)^2)^(3/2) + ',num2str(parameters(2)),'*(R(2))/((R(1))^2+(R(2))^2+(R(3)-',num2str(parameters(1)),'/2)^2)^(3/2)' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionZ = strcat( num2str(parameters(3)),'*(R(3)+',num2str(parameters(1)),'/2)/((R(1))^2+(R(2))^2+(R(3)+',num2str(parameters(1)),'/2)^2)^(3/2) + ',num2str(parameters(2)),'*(R(3)-',num2str(parameters(1)),'/2)/((R(1))^2+(R(2))^2+(R(3)-',num2str(parameters(1)),'/2)^2)^(3/2)' ); % a small value 10^-6 is added to avoid the division by 0 at the origin
            field_BfunctionOK = 1;
            field_Bchosen = BFIELD_MIRROR;
      
    end
    
    % set B field time factor
    switch get(handles.popTimeMagField, 'Value')
        case BFIELD_TIMECONSTANT                                    % constant magnetic field
            field_BTimefunctionX = '1';
            field_BTimefunctionY = '1';
            field_BTimefunctionZ = '1';
        case BFIELD_SINUSOIDAL                                      % sinusoidal magnetic field
            parameters = str2num( get( handles.edtTimeMagField,'String' ));
            field_BTimefunctionX = strcat('sin(',num2str(parameters(1)), '*t+', num2str(parameters(2)), ')');
            field_BTimefunctionY = strcat('sin(',num2str(parameters(1)), '*t+', num2str(parameters(2)), ')');
            field_BTimefunctionZ = strcat('sin(',num2str(parameters(1)), '*t+', num2str(parameters(2)), ')');
        case BFIELD_TIMECUSTOM                                      % custom time factor
            field_BTimefunction = get( handles.edtTimeMagField,'String' );
            [field_BTimefunctionX, remain] = strtok( field_BTimefunction, {',', '[', ']'} );
            [field_BTimefunctionY, remain] = strtok( remain, {',', '[', ']'} );
            [field_BTimefunctionZ, remain] = strtok( remain, {',', '[', ']'} );
    end
    
    % set G field information function as string
    switch get(handles.rbuttGravity, 'Value')
        case GFIELD_NONE
            field_GfunctionZ = '0';                                 % no gravity is present
        
        case GFIELD_CONSTANT
            field_GfunctionZ = '-1000';                              % Earth gravity is present
    end;
            
    
    
      factor_force = 10e11;
      factor_velocity = 10e4;

end