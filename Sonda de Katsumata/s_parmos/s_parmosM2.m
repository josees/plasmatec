function varargout = s_parmosM2(varargin)
% main program
% Version: 10c

%     Copyright (C) 2007 - 2017
%
%       mag. David Erzen
%       Faculty of Mechanical Engineering
%       LECAD Laboratory
%       Askerceva 6
%       1000 Ljubljana
%       SLOVENIA
%       contact email: david.erzen@lecad.uni-lj.si
%       
%       Prof. John P. Verboncoeur
%       Plasma Theory and Simulation Group
%       University of California
%       Berkeley, CA 94720-1730 USA
%       
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%


%**************************************************************************
% MINOR CORRECTIONS in 10c:
% file: simulation_run.m (line: 145-152)
%  guide_center var was not defined vor first 2 steps and MatLab crashed
%  with internal error.
%
%

%**************************************************************************
% MAJOR CORRECTIONS: 
% file: simulation_run.m
% correction: a calcualtion for guiding center is added into the
% calculate_guideCenter procedure
%
% file: simulation_run.m and sim1.wrl
% correction: a guiding center vector is added in sim1.wrl and calculated
% in simulation_run.m
%
% file: s_parmosM2.m
% correction: an option for manipulting with one field line length is
% added. there is a new pannel in the GUI window where user can choose
% electric, or magnetic field, a field line munber and set its length. the
% vrl window stil refreshes when "Show Fields" button is pressed
%
% file: simulation_run.m
% correction: in function calculate_guideCenter the test in if sentence was
% changed. before: if (t(1) ~= t1(1)) && (t(2) ~= t1(2)) &&(t(3) ~= t1(3))
% &&(t1(1) ~= t2(1)) &&(t1(2) ~= t2(2)) && (t1(3) ~= t2(3)) 
% after: if ((t(1) ~= t1(1)) || (t(2) ~= t1(2)) || (t(3) ~= t1(3))) &&
% ((t(1) ~= t2(1)) || (t(2) ~= t2(2)) || (t(3) ~= t2(3))) && ((t2(1) ~=
% t1(1)) || (t2(2) ~= t1(2)) || (t2(3) ~= t1(3))) 
% Before none of the three coordinates should be the same in the three
% points. Now, points should differ form one to another at least in one
% coodrinate.
%
% file: prepare_information.m, s_parmosM2.m, s_parmosM2.fig,
% initialization.m, lobal_variables.m
% correction: an option for concentric magnetic field is added. it is the
% same magnetic field like torus but with a different geometry for field
% lines.
%
% file: global_variables.m
% correction: a Bline_length variable can be changed here for faster
% handling. otherwise this line is under comment therefore just the pannel
% is used to change the field line length
%
% TIME DEPENDENT FIELDS
% fields have time factor with some predefined time functions
% factor is calculated in functions: calculate_B and calculate_E
% visualization of this factor is presented in transparency of field's
% lines, e.g. when factor is 1 lines are not transparent, when factor is 0
% lines are 100% transparent
%

%**************************************************************************


% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @s_parmosM2_OpeningFcn, ...
                   'gui_OutputFcn',  @s_parmosM2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT    
end

% --- Executes just before s_parmosM2 is made visible.
function s_parmosM2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to s_parmosM2 (see VARARGIN)

% Choose default command line output for s_parmosM2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes s_parmosM2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);

    % aditional initialization
    global_variables;
    status = STATUS_STOP;
    Bline_length = [550, 540, 530, 530, 530, 530, 540, 540, 540, 540, 540, 550, 540, 550, 540, 550, 540, 550 ];
    Eline_length = [500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500 ];
    
    
    set(handles.stextMagField, 'Visible', 'Off');
    set(handles.edtMagField, 'Visible', 'Off');
    set(handles.stextElecField, 'Visible', 'Off');
    set(handles.edtElecField, 'Visible', 'Off');
    % added by JK, 2018-10-18
    set(handles.stextTimeMagField, 'Visible', 'Off');
    set(handles.edtTimeMagField, 'Visible', 'Off');
    set(handles.stextTimeElecField, 'Visible', 'Off');
    set(handles.edtTimeElecField, 'Visible', 'Off');
    field_Bchosen = BFIELD_NONE;
    

end

% --- Outputs from this function are returned to the command line.
function varargout = s_parmosM2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end

% --- Executes on selection change in popMagField.
function popMagField_Callback(hObject, eventdata, handles)
% hObject    handle to popMagField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popMagField contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popMagField

    global_variables;
    
    switch get(handles.popMagField, 'Value')
        case BFIELD_NONE                                    % no magnetic field is selected
            set(handles.stextMagField, 'Visible', 'Off');
            set(handles.edtMagField, 'Visible', 'Off');
            
        case BFIELD_CONSTANT                                % constant magnetic filed in z direction
            set(handles.stextMagField, 'Visible', 'On');
            set(handles.stextMagField, 'String', 'Define magnetic field strength [T]');
            set(handles.edtMagField, 'Visible', 'On');
            set(handles.edtMagField, 'String', '0.01');
            
            % lets set also some "nice" particle parameters
            set(handles.edtInitPos, 'String', '[0; 0; 0]');
            set(handles.edtInitVel, 'String', '[0; 3e5; 1e4]');
            set(handles.edtMass, 'String', '1.6e-27');
            set(handles.edtCharge, 'String', '1.6e-19');
            
        case BFIELD_EXPONENT                                % magnetic field in z direction, exponential fall in x direction
            set(handles.stextMagField, 'Visible', 'On');
            set(handles.stextMagField, 'String', 'Equ: Bo*(0,0,exp(-x/L)). Define constants Bo and L');
            set(handles.edtMagField, 'String', '[ 0.025, 0.7 ]');
            set(handles.edtMagField, 'Visible', 'On' );
            
            % lets set also some "nice" particle parameters
            set(handles.edtInitPos, 'String', '[0; 0; 0]');
            set(handles.edtInitVel, 'String', '[0; 3e5; 1e3]');
            set(handles.edtMass, 'String', '1.6e-27');
            set(handles.edtCharge, 'String', '1.6e-19');
            
        case BFIELD_TORUS                                   % magnetic field in a torus
            set(handles.stextMagField, 'String', 'Equ: Bo*Ro(-y, x, 0)/(x^2+y^2). Define Bo, Ro(also torus major radius) and a(torus minor radius)');
            set(handles.stextMagField, 'Visible', 'On');            
             set(handles.edtMagField, 'Visible', 'On');
            set(handles.edtMagField, 'String', '[ 0.1, 2, 1 ]'); 
            
            % lets set also some "nice" particle parameters
            set(handles.edtInitPos, 'String', '[2; 0; 0]');
            set(handles.edtInitVel, 'String', '[0; 3e5; 1e3]');
            set(handles.edtMass, 'String', '1.6e-27');
            set(handles.edtCharge, 'String', '1.6e-19');
            
        case BFIELD_TORUS_POLOIDAL                           % magnetic field in a torus with poloidal magnetic field
            set(handles.stextMagField, 'String', 'Equ: Bo*Ro(-y, x, 0)/(x^2+y^2) + ???????? Define Bt, Ro, Bp and a');
            set(handles.stextMagField, 'Visible', 'On');            
            set(handles.edtMagField, 'Visible', 'On');
            set(handles.edtMagField, 'String', '[ 0.05, 3, -0.01, 1 ]'); 
            
            % lets set also some "nice" particle parameters
            set(handles.edtInitPos, 'String', '[3.8; 0; 0]');
            set(handles.edtInitVel, 'String', '[2e5; 1e5; 2e5]');
            set(handles.edtMass, 'String', '3.67e-27');
            set(handles.edtCharge, 'String', '1.6e-19');
            
        case BFIELD_CONCENTRIC                              % magnetic field in a torus with poloidal magnetic field
            set(handles.stextMagField, 'String', 'Bo*Ro/(x^2+y^2)*[ -y, x, 0 ]  Define Bo and Ro.');
            set(handles.stextMagField, 'Visible', 'On');            
            set(handles.edtMagField, 'Visible', 'On');
            set(handles.edtMagField, 'String', '[ 0.015, 0.5 ]'); 
            
            % lets set also some "nice" particle parameters
            set(handles.edtInitPos, 'String', '[0; 0.5; 0]');
            set(handles.edtInitVel, 'String', '[5e4; 2e5; 0]');
            set(handles.edtMass, 'String', '1.6e-27');
            set(handles.edtCharge, 'String', '1.6e-19');
            
        case BFIELD_CUSTOM                                   % custom magnetic field
            set(handles.stextMagField, 'String', 'Define magnetic field [Bx, By, Bz]. Instead of the coordinates xyz use R(1)R(2)R(3). Example: [ 0, (R(1)+R(2))/Sqrt(R(1)^2+R(2)^2+R(3)^2), R(3)/Sqrt(R(1)^2+R(2)^2+R(3)^2) ]');
            set(handles.stextMagField, 'Visible', 'On');
            set(handles.edtMagField, 'Visible', 'On');
            set(handles.edtMagField, 'String', '[ 0, 0, 0 ]');     
            
       case BFIELD_MIRROR                              % magnetic field in a magnetic mirror / cusp
            set(handles.stextMagField, 'String', 'Define magnetic monopol displacement and top and bottom sign.');
            set(handles.stextMagField, 'Visible', 'On');            
            set(handles.edtMagField, 'Visible', 'On');
            set(handles.edtMagField, 'String', '[ 5, 0.03, -0.03 ]'); 
            
            % lets set also some "nice" particle parameters
            set(handles.edtInitPos, 'String', '[0; 0.1; 0]');
            set(handles.edtInitVel, 'String', '[1e5; 1e5; 1e5]');
            set(handles.edtMass, 'String', '1.6e-27');
            set(handles.edtCharge, 'String', '1.6e-19');

            
    end

end

% --- Executes during object creation, after setting all properties.
function popMagField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popMagField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edtMagField_Callback(hObject, eventdata, handles)
% hObject    handle to edtMagField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtMagField as text
%        str2double(get(hObject,'String')) returns contents of edtMagField as a double
end

% --- Executes during object creation, after setting all properties.
function edtMagField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtMagField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on slider movement.
function sliderForce_Callback(hObject, eventdata, handles)
% hObject    handle to sliderForce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    global_variables;
    value = get(hObject,'Value');
    factor_force = 10^value;
end

% --- Executes during object creation, after setting all properties.
function sliderForce_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderForce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end;
    global_variables;
    value = 13.0;
    set(hObject,'Value', value);
    factor_force = 10^value;
end

% --- Executes on button press in rbuttTraject.
function rbuttTraject_Callback(hObject, eventdata, handles)
% hObject    handle to rbuttTraject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbuttTraject
    global_variables;
    traj_set = get( hObject, 'Value' );
end

% --- Executes on slider movement.
function sliderVel_Callback(hObject, eventdata, handles)
% hObject    handle to sliderVel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
    
    global_variables;
    value = get(hObject,'Value');
    factor_velocity = 10^value;
end

% --- Executes during object creation, after setting all properties.
function sliderVel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderVel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
    global_variables;
    value = 5.0;
    set(hObject,'Value', value);
    factor_velocity = 10^value;
end

% --- Executes on button press in rbuttGuideCenter.
function rbuttGuideCenter_Callback(hObject, eventdata, handles)
% hObject    handle to rbuttGuideCenter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbuttGuideCenter
    global_variables;
    guide_set = get( hObject, 'Value' );
end

% --- Executes on selection change in popElecField.
function popElecField_Callback(hObject, eventdata, handles)
% hObject    handle to popElecField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popElecField contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popElecField

    global_variables;
    
    switch get(handles.popElecField, 'Value')
        case EFIELD_NONE                                % no electric field is selected
            set(handles.stextElecField, 'Visible', 'On');
            set(handles.stextElecField, 'String', 'Time dependent factor');
            set(handles.edtElecField, 'Visible', 'Off');
            
        case EFIELD_CONSTANT                            % constant electric field in x direction
            set(handles.stextElecField, 'Visible', 'On');
            set(handles.stextElecField, 'String', 'Define electric field strength [V/m]');
            set(handles.edtElecField, 'Visible', 'On');
            set(handles.edtElecField, 'String', '100.0');
            
        case EFIELD_CUSTOM                              % custom electric field
            set(handles.stextElecField, 'String', 'Define electric field [Ex, Ey, Ez]. Instead of the coordinates xyz use R(1)R(2)R(3). Example: [ 0, (R(1)+R(2))/Sqrt(R(1)^2+R(2)^2+R(3)^2), R(3)/Sqrt(R(1)^2+R(2)^2+R(3)^2) ]');
            set(handles.stextElecField, 'Visible', 'On');
            set(handles.edtElecField, 'Visible', 'On');
            set(handles.edtElecField, 'String', '[ 0, 0, 0 ]');                           
    end

end

% --- Executes during object creation, after setting all properties.
function popElecField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popElecField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edtElecField_Callback(hObject, eventdata, handles)
% hObject    handle to edtElecField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtElecField as text
%        str2double(get(hObject,'String')) returns contents of edtElecField as a double
end

% --- Executes during object creation, after setting all properties.
function edtElecField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtElecField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edtInitPos_Callback(hObject, eventdata, handles)
% hObject    handle to edtInitPos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtInitPos as text
%        str2double(get(hObject,'String')) returns contents of edtInitPos as a double
end

% --- Executes during object creation, after setting all properties.
function edtInitPos_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtInitPos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edtInitVel_Callback(hObject, eventdata, handles)
% hObject    handle to edtInitVel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtInitVel as text
%        str2double(get(hObject,'String')) returns contents of edtInitVel as a double
end

% --- Executes during object creation, after setting all properties.
function edtInitVel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtInitVel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edtMass_Callback(hObject, eventdata, handles)
% hObject    handle to edtMass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtMass as text
%        str2double(get(hObject,'String')) returns contents of edtMass as a double
end

% --- Executes during object creation, after setting all properties.
function edtMass_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtMass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edtCharge_Callback(hObject, eventdata, handles)
% hObject    handle to edtCharge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtCharge as text
%        str2double(get(hObject,'String')) returns contents of edtCharge as a double
end

% --- Executes during object creation, after setting all properties.
function edtCharge_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtCharge (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on button press in pbuttGoSimulate.
function pbuttGoSimulate_Callback(hObject, eventdata, handles)
% hObject    handle to pbuttGoSimulate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global_variables;
    
    switch status
        case STATUS_STOP
            % start the simulation
            % simulation is stopped and it has to be initialized and
            % restarted
            
            % prepare all simulation information
            prepare_information( handles );
            
            if field_BfunctionOK == 1 & field_EfunctionOK == 1
                % start initialization
                initialization( handles );

                % change the text on button to "Pause Simulation"
                set(handles.pbuttGoSimulate, 'String', 'Pause simulation' );
                pause(0.1);
                % change status to STATUS_RUNNING
                status = STATUS_RUNNING;

                % start simulation
                simulation_run( particle_initVel, particle_initPos, handles );
            end;
            
        case STATUS_RUNNING
            % pause simulation
            % it pause or stop with the status change
            
            % change the text on button to "Restart Simulation"
            set(handles.pbuttGoSimulate, 'String', 'Restart simulation' );
            pause(0.1);
            % change status to STATUS_PAUSED
            status = STATUS_PAUSED;
            
            % save the world for further applications
            vrdrawnow;
            save(w, get(w, 'FileName'));            
            
        case STATUS_PAUSED
            % change the text on button to "Pause Simulation"            
            set(handles.pbuttGoSimulate, 'String', 'Pause simulation' );
            pause(0.1);
            % change status to STATUS_RUNNING
            status = STATUS_RUNNING;
            
            % unpause simulation         
            simulation_run( particle_vel, particle_pos, handles );  
            
        case STATUS_FIELDS_SET
            % change the text on button to "Pause Simulation"
            set(handles.pbuttGoSimulate, 'String', 'Pause simulation' );
            pause(0.1);
            % change status to STATUS_RUNNING
            status = STATUS_RUNNING;
            
            % start simulation
            simulation_run( particle_initVel, particle_initPos, handles );
    end
end

% --- Executes on button press in pbuttStopSimulation.
function pbuttStopSimulation_Callback(hObject, eventdata, handles)
% hObject    handle to pbuttStopSimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    global_variables;

    switch status
        case STATUS_STOP
            % don't change anything
            
        case STATUS_RUNNING
            %offer saving simulation
            
            % change the text on start button "Start Simulation"            
            set(handles.pbuttGoSimulate, 'String', 'Start simulation' );            
            
            % change status to STATUS_STOP
            status = STATUS_STOP;
            
        case STATUS_PAUSED
            % stop simulation
            
            %offer saving simulation
            
            % change the text on start button "Start Simulation"            
            set(handles.pbuttGoSimulate, 'String', 'Start simulation' );
            
            % change status to STATUS_STOP
            status = STATUS_STOP;
    end
    % save the vr world for further applications
    vrdrawnow;
    save(w, get(w, 'FileName'));
end



% --- Executes on button press in rbuttonVectorForce.
function rbuttonVectorForce_Callback(hObject, eventdata, handles)
% hObject    handle to rbuttonVectorForce (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbuttonVectorForce
    global_variables;
    vectorForce_set = get( hObject, 'Value' );
end

% --- Executes on button press in rbuttonVectorVelocity.
function rbuttonVectorVelocity_Callback(hObject, eventdata, handles)
% hObject    handle to rbuttonVectorVelocity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbuttonVectorVelocity
    global_variables;
    vectorVel_set = get( hObject, 'Value' );
end

% --- Executes on button press in pbuttShowFields.
function pbuttShowFields_Callback(hObject, eventdata, handles)
% hObject    handle to pbuttShowFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    global_variables;
    
    
    % prepare all simulation information
    prepare_information( handles );

    % start initialization
    initialization( handles );
    
    status = STATUS_FIELDS_SET;
end


% --- Executes on button press in rbuttonGuidingCenterVector.
function rbuttonGuidingCenterVector_Callback(hObject, eventdata, handles)
% hObject    handle to rbuttonGuidingCenterVector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbuttonGuidingCenterVector

    global_variables;
    vectorGuide_set = get( hObject, 'Value' );
end


% --------------------------------------------------------------------
function SaveData_Callback(hObject, eventdata, handles)
% hObject    handle to SaveData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global_variables;
    
    [file,path] = uiputfile;
    file_info = strcat(path, file);
    if strcmp(file, '') == 0
        % saving particle trajectory
        save_file = fopen(file_info,'w');
        fprintf(save_file,'particle trajectory \n');
        fclose(save_file);
        save(file_info, 'trajectory', '-ascii', '-append');
        
        % saving guiding center
        save_file = fopen(file_info,'a');
        fprintf(save_file,'\n guiding center \n');
        fclose(save_file);
        save(file_info, 'guide_center', '-ascii', '-append');
        
        % saving magnetic field lines
        save_file = fopen(file_info,'a');
        fprintf(save_file,'\n magnetic field lines \n');
        fclose(save_file);
        for i = 1:18                          % TODO: change the number to a variable!!!
            save_file = fopen(file_info,'a');
            fprintf(save_file,'\n B line number %d \n', i);
            fclose(save_file);
            if i <= 9
                tmp_num = i;
            else
                tmp_num = i+1;
            end
            name_line = strcat('B', num2str(tmp_num), '_spine');
            line = vrnode(w, name_line); 
            spine_data = line.spine;
            save(file_info,'spine_data', '-ascii', '-append');            
        end;                    
        
        % saving electric field lines
        fprintf(save_file,'\n electric field lines \n');
        for i = 1:18                          % TODO: change the number to a variable!!!
            save_file = fopen(file_info,'a');
            fprintf(save_file,'\n E line number %d \n', i);
            fclose(save_file);
            if i <= 9
                tmp_num = i;
            else
                tmp_num = i+1;
            end
            name_line = strcat('E', num2str(tmp_num), '_spine');
            line = vrnode(w, name_line); 
            spine_data = line.spine;
            save(file_info,'spine_data', '-ascii', '-append');            
        end;                            
        
    end;
    
end


% --- Executes on selection change in popLinenum.
function popLinenum_Callback(hObject, eventdata, handles)
% hObject    handle to popLinenum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popLinenum contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popLinenum

    global_variables;
    
    switch get(handles.popFields, 'Value')
        case EFIELD_LINE
            set(handles.edtLinelenght, 'String', Eline_length(get(hObject, 'Value')));
        case BFIELD_LINE
            set(handles.edtLinelenght, 'String', Bline_length(get(hObject, 'Value')));
    end;

end

% --- Executes during object creation, after setting all properties.
function popLinenum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popLinenum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end



% --- Executes on selection change in popFields.
function popFields_Callback(hObject, eventdata, handles)
% hObject    handle to popFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popFields contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popFields

    global_variables;
    
    switch get(hObject, 'Value')
        case EFIELD_LINE
            set(handles.edtLinelenght, 'String', Eline_length(get(handles.popLinenum, 'Value')));
        case BFIELD_LINE
            set(handles.edtLinelenght, 'String', Bline_length(get(handles.popLinenum, 'Value')));
    end;

end


% --- Executes during object creation, after setting all properties.
function popFields_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popFields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end




function edtLinelenght_Callback(hObject, eventdata, handles)
% hObject    handle to edtLinelenght (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtLinelenght as text
%        str2double(get(hObject,'String')) returns contents of edtLinelenght as a double
    global_variables;
    
    switch get(handles.popFields, 'Value')
        case EFIELD_LINE
            Eline_length(get(handles.popLinenum, 'Value')) = str2num(get(hObject, 'String'));
        case BFIELD_LINE
            Bline_length(get(handles.popLinenum, 'Value')) = str2num(get(hObject, 'String'));
    end;

end

% --- Executes during object creation, after setting all properties.
function edtLinelenght_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtLinelenght (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

end


% --- Executes on selection change in popTimeMagField.
function popTimeMagField_Callback(hObject, eventdata, handles)
% hObject    handle to popTimeMagField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popTimeMagField contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popTimeMagField

    global_variables;
    
    switch get(handles.popTimeMagField, 'Value')
        case BFIELD_TIMECONSTANT                                    % constant magnetic field
            set(handles.stextTimeMagField, 'Visible', 'Off');
            set(handles.edtTimeMagField, 'Visible', 'Off');
        case BFIELD_SINUSOIDAL                                      % sinusoidal magnetif field
            set(handles.stextTimeMagField, 'Visible', 'On');
            set(handles.stextTimeMagField, 'String', 'Define constants [ w, f ]');
            set(handles.edtTimeMagField, 'Visible', 'On');
            set(handles.edtTimeMagField, 'String', '[ 1e4; 0 ]');
        case BFIELD_TIMECUSTOM                                      % custom time factor
            set(handles.stextTimeMagField, 'Visible', 'On');
            set(handles.stextTimeMagField, 'String', 'Define factors for each direction [ f(t), g(t), h(t) ]');
            set(handles.edtTimeMagField, 'Visible', 'On');
            set(handles.edtTimeMagField, 'String', '[ sin(1e3*t), cos(3e3*t+1.2), atan(t-pi/3) ]');
    end
end

% --- Executes during object creation, after setting all properties.
function popTimeMagField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popTimeMagField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edtTimeMagField_Callback(hObject, eventdata, handles)
% hObject    handle to edtTimeMagField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtTimeMagField as text
%        str2double(get(hObject,'String')) returns contents of edtTimeMagField as a double

end


% --- Executes during object creation, after setting all properties.
function edtTimeMagField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtTimeMagField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function edtTimeElecField_Callback(hObject, eventdata, handles)
% hObject    handle to edtTimeElecField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edtTimeElecField as text
%        str2double(get(hObject,'String')) returns contents of edtTimeElecField as a double

end


% --- Executes during object creation, after setting all properties.
function edtTimeElecField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edtTimeElecField (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% --- Executes on selection change in popTimeElecfield.
function popTimeElecField_Callback(hObject, eventdata, handles)
% hObject    handle to popTimeElecfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popTimeElecfield contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popTimeElecfield
    global_variables;
    
    switch get(handles.popTimeElecField, 'Value')
        case EFIELD_TIMECONSTANT                                    % constant electric field
            set(handles.stextTimeElecField, 'Visible', 'Off');
            set(handles.edtTimeElecField, 'Visible', 'Off');
        case EFIELD_SINUSOIDAL                                      % sinusoidal electric field
            set(handles.stextTimeElecField, 'Visible', 'On');
            set(handles.stextTimeElecField, 'String', 'Define constants [ w, f ]');
            set(handles.edtTimeElecField, 'Visible', 'On');
            set(handles.edtTimeElecField, 'String', '[ 1e4; 0 ]');
        case EFIELD_TIMECUSTOM                                      % custom time factor
            set(handles.stextTimeElecField, 'Visible', 'On');
            set(handles.stextTimeElecField, 'String', 'Define factors for each direction [ f(t), g(t), h(t) ]');
            set(handles.edtTimeElecField, 'Visible', 'On');
            set(handles.edtTimeElecField, 'String', '[ sin(1e3*t), cos(3e3*t+1.2), atan(t-pi/3) ]');
    end

end


% --- Executes during object creation, after setting all properties.
function popTimeElecField_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popTimeElecfield (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


end



% --- Executes on button press in rbuttGravity.
function rbuttGravity_Callback(hObject, eventdata, handles)
% hObject    handle to rbuttGravity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbuttGravity
            
    set(handles.edtInitPos, 'String', '[0; 0; 0]');
    set(handles.edtInitVel, 'String', '[0; 2e1; 0]');
end



% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pbuttGoSimulate.
function pbuttGoSimulate_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pbuttGoSimulate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end
