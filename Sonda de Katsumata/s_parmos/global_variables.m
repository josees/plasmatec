% global variables used in almost every function



%     Copyright (C) 2007 
%
%       mag. David Erzen
%       Faculty of Mechanical Engineering
%       LECAD Laboratory
%       Askerceva 6
%       1000 Ljubljana
%       SLOVENIA
%       contact email: david.erzen@lecad.uni-lj.si
%       
%       Prof. John P. Verboncoeur
%       Plasma Theory and Simulation Group
%       University of California
%       Berkeley, CA 94720-1730 USA
%       
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
    



% length of magnetic field lines
global Bline_length;
 %Bline_length = [640, 1, 640, 1, 640, 1, 640, 1, 640, 640, 1, 640, 1, 640, 1, 640, 1, 640 ];
 %Bline_length = [126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400, 126400 ];
 %Bline_length = [12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640 ];
 %Bline_length = [626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626 ];
 %Bline_length = [786, 786, 786, 786, 786, 786, 786, 786, 786, 1126, 1126, 1126, 1126, 1126, 1126, 1126, 1126, 1126 ];
 %Bline_length = [26, 26, 26, 26, 26, 26, 26, 26, 26, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1 ];
 %Bline_length = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1 ];
 %Bline_length = [626, 626, 626, 626, 626, 626, 626, 626, 626, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000 ];
 %Bline_length = [60, 60, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ];
 
% length of electric field lines
global Eline_length;
%Eline_length = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1 ];
%Eline_length = [626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626, 626 ];
 %Eline_length = [12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640 ];

% simualtion status
global STATUS_STOP;
STATUS_STOP = 0;
global STATUS_RUNNING;
STATUS_RUNNING = 1;
global STATUS_PAUSED;
STATUS_PAUSED = 2;
global STATUS_FIELDS_SET;
STATUS_FIELDS_SET = 3;

global status;

% simulation info
global particle_charge;     %particle property - charge
global particle_initVel;    %particle property - initial velocity
global particle_initPos;    %particle property - initial position
global particle_mass;       %particle property - mass

% ELECTRIC FIELD INFO *********************************************
global EFIELD_NONE          % no electric field
EFIELD_NONE = 1;            
global EFIELD_CONSTANT;     % constant electric field in x direction
EFIELD_CONSTANT = 2;
global EFIELD_CUSTOM;       % custom electric field by user
EFIELD_CUSTOM = 3;

global field_Echosen;       % chosen electric field in simulation

global EFIELD_TIMECONSTANT  % constant electric field in time
EFIELD_TIMECONSTANT = 1;
global EFIELD_SINUSOIDAL    % sinusoidal electric field in time
EFIELD_SINUSOIDAL = 2;
global EFIELD_TIMECUSTOM    % custom defined behaviour of electric field in time
EFIELD_TIMECUSTOM = 3;

global field_Efunction;     % vector function for electric field
global field_EfunctionX;    % function for electric field - x component
global field_EfunctionY;    % function for electric field - y component
global field_EfunctionZ;    % function for electric field - z component

global field_EfunctionOK;   % is 1 if function is set correctly and 0 if not

global field_ETimefunctionX % function for time factor for electric field - x direction
global field_ETimefunctionY % function for time factor for electric field - y direction
global field_ETimefunctionZ % function for time factor for electric field - z direction


% MAGNETIC FIELD INFO *******************************************
BFIELD_NONE = 1;
global BFIELD_CONSTANT;     % constant magnetic field in z direction
BFIELD_CONSTANT = 2;
global BFIELD_EXPONENT;     % magnetic field in z direction falling exponentialy with x coordinate
BFIELD_EXPONENT = 3;
global BFIELD_TORUS;        % magnetic field in a torus
BFIELD_TORUS = 4;
global BFIELD_TORUS_POLOIDAL; % magnetic field in a torus with poloidal component
BFIELD_TORUS_POLOIDAL = 5;
global BFIELD_CONCENTRIC;   % magnetic field in a concentric geometry
BFIELD_CONCENTRIC = 6;
global BFIELD_CUSTOM;       % custom magnetic field by user
BFIELD_CUSTOM = 7;
global BFIELD_MIRROR;       % magnetic field of a m. mirror or cusp, depends of monopole signs
BFIELD_MIRROR = 8;

global field_Bchosen;       % chosen magnetic field in simulation

global BFIELD_TIMECONSTANT  % constant magnetic field in time
BFIELD_TIMECONSTANT = 1;
global BFIELD_SINUSOIDAL    % sinusoidal magnetic field in time
BFIELD_SINUSOIDAL = 2;
global BFIELD_TIMECUSTOM    % custom defined behaviour of magnetic field in time
BFIELD_TIMECUSTOM = 3;

global field_Bfunction;     % vector function for magnetic field
global field_BfunctionX;    % function for magnetic field - x direction
global field_BfunctionY;    % function for magnetic field - y direction
global field_BfunctionZ;    % function for magnetic field - z direction

global field_BfunctionOK;   % is 1 if function is set correctly and 0 if not

global field_BTimefunctionX % function for time factor for magnetic field - x direction
global field_BTimefunctionY % function for time factor for magnetic field - y direction
global field_BTimefunctionZ % function for time factor for magnetic field - z direction

% field lines
global EFIELD_LINE;         % electric field selection
EFIELD_LINE = 1;
global BFIELD_LINE;         % magnetic field selection
BFIELD_LINE = 2;

% GRAVITATIONAL FIELD INFO *******************************************
global GFIELD_NONE;
GFIELD_NONE = 0;             % no gravitational field is present
global  GFIELD_CONSTANT;
GFIELD_CONSTANT = 1;             % gravitational field is present ATM just EARTH G FIELD :)

global field_GfunctionZ     % function for gravitational field  - z direction

% trajectory and guiding center
global trajectory;          % trajectory matrix
global guide_center;        % guiding center matrix
global traj_set;            % enable/disable trajectory to be drawn
global guide_set;           % enable/disable guiding center to be drawn
global particle_vel;        % temporary particle velocity
global particle_pos;        % temporary particle position

% force and velocity vectors
global factor_force;        % force factor for drawing force vector
global factor_velocity;     % velocity factor for drawinf velocity vector
global vectorForce_set;     % enable/disable force vector to be drawn
global vectorVel_set;       % enable/disable velocity vector to be drawn
global vectorGuide_set;     % enable/disable guiding center vector to be drawn

% virtual reality world
global w;                   % virtual reality file

% absolute time in simulation
global absolute_time;