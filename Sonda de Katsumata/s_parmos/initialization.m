function initialization( handles )
% initializes the vr world


%     Copyright (C) 2007 
%
%       mag. David Erzen
%       Faculty of Mechanical Engineering
%       LECAD Laboratory
%       Askerceva 6
%       1000 Ljubljana
%       SLOVENIA
%       contact email: david.erzen@lecad.uni-lj.si
%       
%       Prof. John P. Verboncoeur
%       Plasma Theory and Simulation Group
%       University of California
%       Berkeley, CA 94720-1730 USA
%       
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
    


    global_variables;

    % calculate E field and B field lines
    absolute_time = -1;
    [Bfield_lines Efield_lines] = BE_splines( handles );
    
    % set virtual world
    make_vr( 'sim1.wrl', Bfield_lines, Efield_lines );
    % TODO: the name of the virtual world file can be optional
    
    % set trajectory and guide center memory initial points
    trajectory = particle_initPos;
    clear global guide_center;    
    traj_set = get( handles.rbuttTraject, 'Value' ); 
    guide_set = get( handles.rbuttGuideCenter, 'Value' );
    vectorForce_set = get( handles.rbuttonVectorForce, 'Value' );
    vectorVel_set = get( handles.rbuttonVectorVelocity, 'Value' );
end

% ********** calculate magnetic and electric field lines ******************
function [Bfield_lines Efield_lines] = BE_splines( handles )

    global_variables;

    % initialize magnetic field lines
    B_parameters = str2num( get( handles.edtMagField,'String' ) );    
        
    switch get( handles.popMagField,'Value' )
        case BFIELD_NONE                                            % no magnetic field is selected
            spline_pos = [ 0 0 0.1; 1 0 -1; 1 1 0; 0 1 1; -1 1 2; -1 0 1; -1 -1 0; 0 -1 -1; 1 -1 -2; 0 0 0.1; 1 0 -1; 1 1 0; 0 1 1; -1 1 2; -1 0 1; -1 -1 0; 0 -1 -1; 1 -1 -2 ]; % starting point of splines, which represent field lines                
            Bline_length = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ];

        case BFIELD_CONSTANT                                        % constant magnetic filed in z direction
            spline_pos = [ 0 0 0.1; 1 0 -1; 1 1 0; 0 1 1; -1 1 2; -1 0 1; -1 -1 0; 0 -1 -1; 1 -1 -2; 0 0 0.1; 1 0 -1; 1 1 0; 0 1 1; -1 1 2; -1 0 1; -1 -1 0; 0 -1 -1; 1 -1 -2 ]; % starting point of splines, which represent field lines    
            Bline_length = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 ];

        case BFIELD_EXPONENT                                        % magnetic field in z direction, exponential fall in x direction
            spline_pos = [ 0 0 0.1; 1 0 -1; 1 1 0; 0 1 1; -1 1 2; -1 0 1; -1 -1 0; 0 -1 -1; 1 -1 -2; 0 0 0.1; 1 0 -1; 1 1 0; 0 1 1; -1 1 2; -1 0 1; -1 -1 0; 0 -1 -1; 1 -1 -2 ]; % starting point of splines, which represent field lines    
            Bline_length = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 ];
        
        case BFIELD_TORUS                                           % magnetic field in a torus           
            spline_pos = zeros(18, 3);
            temp_radius = zeros(18,1);
            for i=1:9
                spline_pos(i,1:3) = [B_parameters(2) + B_parameters(3)*cos( 2*pi*i/9 ) , 0,   B_parameters(3)*sin( 2*pi*i/9 ) ];
                temp_radius(i,1) = sqrt(spline_pos(i,1)^2 + spline_pos(i,3)^2);
            end            
            spline_pos(10:18, 1:3) = spline_pos(1:9, 1:3);  
            spline_pos(10:18, 1) = -spline_pos(1:9, 1);     % made for
            %calculating magnetic field lines in opposite direction
            temp_radius(10:18, 1) = temp_radius(1:9,1);
            
            Bline_length = 40*temp_radius;
        
        case BFIELD_TORUS_POLOIDAL                                  % magnetic field in a torus with poloidal field
            for i=1:9
                spline_pos(i,1:3) = [B_parameters(2) - B_parameters(4)*cos( pi/2+pi*i/9 ) , 0,   B_parameters(4)*sin( pi/2+pi*i/9 ) ];
                % temporary positions:
                spline_pos(i+9,1:3) = [-B_parameters(2) - B_parameters(4)*cos( -pi/2 + pi*i/9 ) , 0,   B_parameters(4)*sin( -pi/2 + pi*i/9 ) ];
            end
            % spline_pos(10:18, 1:3) = spline_pos(1:9, 1:3); 
            % spline_pos(10:18, 1) = -spline_pos(1:9, 1);     % made for
            % calculating magnetic field lines in opposite direction
            Bline_length = 1.6*(B_parameters(2) +B_parameters(4) )* [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 ];
           
        case BFIELD_CONCENTRIC
            spline_pos = [ B_parameters(2) 0 0.1; B_parameters(2) 0 1; B_parameters(2) 0 2; B_parameters(2) 0 3; B_parameters(2) 0 4; B_parameters(2) 0 -1; B_parameters(2) 0 -2; B_parameters(2) 0 -3; B_parameters(2) 0 -4; 3*B_parameters(2) 0 0.1; 3*B_parameters(2) 0 1; 3*B_parameters(2) 0 2; 3*B_parameters(2) 0 3; 3*B_parameters(2) 0 4; 3*B_parameters(2) 0 -1; 3*B_parameters(2) 0 -2; 3*B_parameters(2) 0 -3; 3*B_parameters(2) 0 -4]; % starting point of splines, which represent field lines    
            Bline_length = 12*B_parameters(2)*[100, 100, 100, 100, 100, 100, 100, 100, 100, 300, 300, 300, 300, 300, 300, 300, 300, 300 ];
            
        case BFIELD_CUSTOM                                          % a user defined magnetic field is selected 
            %spline_pos = [ 0.01 0.01 3.01; 0.01 -0.01 3.01; -0.01 -0.01 3.01; -0.01 0.01 3.01; 0.01 0.01 2.99; 0.01 -0.01 2.99; -0.01 0.01 2.99; -0.01 -0.01 2.99; 0.01 -0.01 2.99; 0.01 0.01 3; -0.01 0.01 3; -0.01 -0.01 3; 0 -0.01 2.99; 0 0.01 2.99 ; 0 0.01 3.01; -0.01 -0.01 3; 0 -0.01 2.99; 0 0 3.01 ]; % starting point of splines, which represent field lines            
            %spline_pos = [ 0.001 0.001 3.01; 0.001 -0.001 3.01; -0.001 -0.001 3.01; -0.001 0.001 3.01; 0.001 0.001 7.99; 0.001 -0.001 7.99; -0.001 0.001 7.99; -0.001 -0.001 7.99; 0.001 -0.001 7.99; 0.001 0.001 3.01; -0.001 0.001 3.01; -0.001 -0.001 3.01; 0 -0.001 7.99; 0 0.001 7.99 ; 0 0.001 3.01; -0.001 -0.001 3; 0 -0.001 7.99; 0.001 0 3.01 ]; % starting point of splines, which represent field lines            
            %spline_pos = [ 1.0015 1.0015 0.01; 1.0015 0.9985 0.01; 0.9985 0.9985 0.01; 0.9985 1.0015 0.01; 1.0015 1.0015 4.99; 1.0015 0.9985 4.99; 0.9985 1.0015 4.99; 0.9985 0.9985 4.99; 1.0015 0.9985 4.99; 1.0015 1.0015 0.01; 0.9985 1.0015 0.01; 0.9985 0.9985 0.01; 1 0.9985 4.99; 1 1.0015 4.99 ; 1 1.0015 0.01; 1.0 1.0 8; 1 0.9985 4.99; 1.0015 1 0.01 ];
            %spline_pos = [ 1 1.001 0.008; 1.0 0.999 0.008; 1.0 1.0 0.008; 1.001 1.001 0.008; 1.001 0.999 0.008; 0.999 0.999 0.008; 0.999 1.001 0.008; 1.001 1.0 0.008; 0.999 1.0 0.008; 1.005 1.005 0.008; 1.005 0.995 0.008; 0.995 1.005 0.008; 0.995 0.995 0.008; 1.005 1.0 0.008; 0.995 1.0 0.008; 1 0.995 0.008; 1 1.005 0.008 ; 1.0 1.0 5.995]; % starting point of splines, which represent field lines            
            spline_pos = [ 0 0 0.1; 1 0 -1; 1 1 0; 0 1 1; -1 1 2; -1 0 1; -1 -1 0; 0 -1 -1; 1 -1 -2; 0 0 0.1; 1 0 -1; 1 1 0; 0 1 1; -1 1 2; -1 0 1; -1 -1 0; 0 -1 -1; 1 -1 -2 ]; % starting point of splines, which represent field lines  
            %spline_pos = [ 0.1 0.1 3.1; 0.1 -0.1 3.1; -0.1 -0.1 3.1; -0.1 0.1 3.1; 0.1 0.1 2.9; 0.1 -0.1 2.9; -0.1 0.1 2.9; -0.1 -0.1 2.9; 0.1 -0.1 2.9; 0.1 0.1 3; -0.1 0.1 3; -0.1 -0.1 3; 0 -0.1 2.9; 0 0.1 2.9 ; 0 0.1 3.1; -0.1 -0.1 3; 0 -0.1 2.9; 0 0 3.1 ]; % starting point of splines, which represent field lines            
            Bline_length = [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 ];
            
        case BFIELD_MIRROR
            dl = 0.1;
            dz = 0.3;
            spline_pos = [ 0, 0, B_parameters(1)/2-dz; dl, 0, B_parameters(1)/2-dz; 0, dl, B_parameters(1)/2-dz; -dl, 0, B_parameters(1)/2-dz; 0, -dl, B_parameters(1)/2-dz; 0, 0, -B_parameters(1)/2+dz; dl, 0, -B_parameters(1)/2+dz; 0, dl, -B_parameters(1)/2+dz; -dl, 0, -B_parameters(1)/2+dz; 0, -dl, -B_parameters(1)/2+dz; dl, dl, B_parameters(1)/2-dz; -dl, dl, B_parameters(1)/2-dz; -dl, -dl, B_parameters(1)/2-dz; dl, -dl, B_parameters(1)/2-dz; dl, dl, -B_parameters(1)/2+dz; -dl, dl, -B_parameters(1)/2+dz; -dl, -dl, -B_parameters(1)/2+dz; dl, -dl, -B_parameters(1)/2+dz ];
            
            if B_parameters(2) > 0  % only if moonpole is positive a field line should begin near by otherwise it will collapse into the monopole
                top_length = 1;
            else
                top_length = 1e-6;
            end;
            
            if B_parameters(3) > 0 
                bottom_length = 1;
            else
                bottom_length = 1e-6;
            end;         
                
            Bline_length = B_parameters(1) * 100 * [top_length, top_length, top_length, top_length, top_length, bottom_length, bottom_length, bottom_length, bottom_length, bottom_length, top_length, top_length, top_length, top_length, bottom_length, bottom_length, bottom_length, bottom_length ];
            
        otherwise
           spline_pos = [ -1 1 1; 2 0 -1; 2 2 0; 0 2 1; -2 2 2; -2 0 1; -2 -2 0; 0 -2 -1; 2 -2 -2; 1 1 -1; -2 0 -1; -2 2 0; 0 -2 1; -2 2 2; 2 0 1; 2 -2 0; 0 2 -1; 2 -2 -2 ]; % starting point of splines, which represent field lines    
           %spline_pos = [ 2.6,0,0;094,0,0.034202;0766,0,0.064279;05,0,0.086603;0174,0,0.098481;2.4826,0,0.098481;2.45,0,0.086603;2.4234,0,0.064279;2.406,0,0.034202;2.4,0,1.2246e-017;2.406,0,-0.034202;2.4234,0,-0.064279;2.45,0,-0.086603;2.4826,0,-0.098481;0174,0,-0.098481;05,0,-0.086603;0766,0,-0.064279;094,0,-0.034202];
           Bline_length = 0.2*[100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100 ];
    end
    
    if get( handles.popMagField, 'Value' ) > 1    % if any of magnetic field is chosen
        for i = 1:length( spline_pos )
            if i <= length( spline_pos )/2     % second half of spline_pos point are the same like first half, but they are used to generate spline in other directions
                direction = 1;
            else
                direction = -1;
            end
            Bfield_lines(i) = struct( 'spline', Calculate_spline( spline_pos(i,1:3),handles, direction, BFIELD_LINE, i, Bline_length(i) ) );
        end
    else                                     % if no magnetic field is chosen
        for i = 1:length( spline_pos )
            Bfield_lines(i) = struct( 'spline', []);
        end
    end;
    
    % initialize electric field lines; the same like magnetic field
    
    %spline_pos = [ 0 0 0.1; 2 0 -2; 2 2 0; 0 2 2; -2 2 3; -2 0 2; -2 -2 0; 0 -2 -2; 2 -2 -3; 0 0 0.1; 2 0 -2; 2 2 0; 0 2 2; -2 2 3; -2 0 2; -2 -2 0; 0 -2 -2; 2 -2 -3 ];
    spline_pos = [ 0 0 0; 0 1 0; 0 1 1; 0 0 1; 0 -1 1; 0 -1 0; 0 -1 -1; 0 0 -1; 0 1 -1; 0 2 0; 0 2 2; 0 0 2; 0 -2 2; 0 -2 0; 0 -2 -2; 0 0 -2; 0 2 -2; 0 0 3 ]; % starting point of splines, which represent field lines 
    %spline_pos = [ -50 -100 -10.1; -40 -100 -10; -40 -101 -10; -50 -101 -10; -60 -101 -10; -60 -100 -10; -60 -100 -10; -50 -100 -10; -40 -100 -10; -50 -100 -10; -40 -100 -10; -40 -101 -10; -50 -101 -10; -60 -101 -10; -60 -100 -10; -60 -101 -10; -50 -100 -10; -40 -100 -10 ]; % starting point of splines, which represent field lines  
    Eline_length = [12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640, 12640 ];

    if get( handles.popElecField, 'Value' ) > 1      % if any of electric field is chosen  
        for i = 1:length( spline_pos )
            if i <= length(spline_pos)/2             % second half of spline_pos point are the same like first half, but they are used to generate spline in other directions
                direction = 1;
            else
                direction = -1;
            end
            Efield_lines(i) = struct( 'spline', Calculate_spline( spline_pos(i,1:3),handles, direction, EFIELD_LINE, i, Eline_length(i) ) );
        end
    else                                            % if no electric field is chosen
        for i = 1:length( spline_pos )
            Efield_lines(i) = struct( 'spline', [] );
        end
    end;
end

% ******** calculates spline ***********************************************
function spline = Calculate_spline( position, handles, direction, field, line_num, line_length )
 % calculates one spline of magnetic or electric field. the parameter
 % direction is not in use atm
    global_variables;
    absolute_time = -1;
    
    switch field
        case EFIELD_LINE
            [l,spline] = ode15s( @calculate_E,[0 line_length],position );
            
        case BFIELD_LINE
            [l,spline] = ode15s( @calculate_B,[0 line_length],position );
    end
    
end

% *********** sets virtual reality world magnetic and electric field lines
function w = make_vr(simfile, Bfield_lines, Efield_lines);

    w = vrworld(simfile);  % updating virtual world
    open(w);
    temp = get(w,'Figures');
    if  length(temp) == 0
        view(w);
        set(w,'TimeSource','external');
        %view(w, '-web');
    end
    
    alpha = [0: pi./10 : 2*pi]';                        
    cross = 0.01*[cos(alpha) -sin(alpha)];

    for i = 1:length(Bfield_lines)              % updating magnetic field lines
        if i <= length(Bfield_lines)/2 
            tmp_num = i;
        else
            tmp_num = i+1;
        end
        name_line = strcat('B', num2str(tmp_num), '_spine');
        line = vrnode(w, name_line);
        if length(Bfield_lines(i).spline) > 0
            %temp1 = field_lines(i).spline;
            line.spine = Bfield_lines(i).spline;
        else
            line.spine = [];
        end;
        line.crossSection = cross;                        
        % TODO: crossSection of a magnetic field line could represent the
        % strength of the magnetic field in the particular area
        line.solid = true;            
    end;
    
    
     for i = 1:length(Efield_lines)              % updating electric field lines
        if i <= length(Efield_lines)/2 
            tmp_num = i;
        else
            tmp_num = i+1;
        end
        name_line = strcat('E', num2str(tmp_num), '_spine');
        line = vrnode(w, name_line);
        if length(Efield_lines(i).spline) > 0
            %temp1 = field_lines(i).spline;
            line.spine = Efield_lines(i).spline;
        else
            line.spine = [];
        end;
        line.crossSection = cross;                        
        % TODO: crossSection of a electric field line could represent the
        % strength of the electric field in the particular area
        line.solid = true;            
    end;
    
    w.trajectory_spine.spine = [0 0 0; 0 0 0];
    w.trajectory_spine.crossSection = cross;
    
    w.guide_spine.spine = [0 0 0; 0 0 0];
    w.guide_spine.crossSection = cross;
    
    save(w, get(w, 'FileName'));

    vrdrawnow;

end