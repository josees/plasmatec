const int pinDireccion = 3; 
const int pinPasos=4;
const int pinHorario=5;
const int pinAntihorario=6;
const int pinIniciar=9;
const int pinCalibrar=8;
const int pinRapidezplus =12; 
const int pinRapidezminus=13;

//25 grados de giro
const int pasos = 5;

boolean activo=false;
int retraso = 7; //10 milisegundos de espera

void setup() {
  pinMode(pinPasos,OUTPUT); 
  pinMode(pinDireccion,OUTPUT);
  pinMode(pinIniciar,INPUT);
  pinMode(pinCalibrar,INPUT);
  pinMode(pinHorario,INPUT);
  pinMode(pinAntihorario,INPUT);

}

void loop() {
  //-----------------------Solo si no está calibrando------------------
  if(activo){
    digitalWrite(pinDireccion,HIGH);
    for(int i = 0; i < pasos; i++) {
      digitalWrite(pinPasos,HIGH); 
      delay(retraso); 
      digitalWrite(pinPasos,LOW); 
      delay(retraso); 
    }
    delay(100); // One second delay
    
    digitalWrite(pinDireccion,LOW);
    for(int i = 0; i < pasos; i++) {
      digitalWrite(pinPasos,HIGH);
      delay(retraso); 
      digitalWrite(pinPasos,LOW);
      delay(retraso); 
    }
    delay(100);
  }
  
  //----------------------Lectura de botones------------------------
  if(digitalRead(pinIniciar) == HIGH){
    activo = true;
  }
  if(digitalRead(pinCalibrar) == HIGH){
    activo = false;  
  }
  if(digitalRead(pinRapidezplus) == HIGH){
    if(retraso!=1){
      --retraso; 
    }
  }
  if(digitalRead(pinRapidezminus) == HIGH){
    ++retraso;
  }
  
  if(digitalRead(pinHorario) == HIGH){
     digitalWrite(pinDireccion,HIGH);
     digitalWrite(pinPasos,HIGH); 
     delay(40); 
     digitalWrite(pinPasos,LOW);
     digitalWrite(pinDireccion,LOW); 
     delay(40);
     
  }else if(digitalRead(pinAntihorario) == HIGH){
     digitalWrite(pinDireccion,LOW);
     digitalWrite(pinPasos,HIGH); 
     delay(40); 
     digitalWrite(pinPasos,LOW); 
     delay(40);
  }

  



  
}
