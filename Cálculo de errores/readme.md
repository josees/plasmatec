# Cálculo de Errores + Estimación de las superficies transversal y toroidal y el volumen del plasma

Esta carpeta contiene cuadernos de Jupyter en los que se desarrollan diversos cálculos asociados a la estimación de áreas transversales, toroidales y el volumen de plasma. Además, contiene métodos de estimación del flujo del plasma a través de la superficie transversal encerrada por un camino toroidal seleccionado.

## Cálculo de errores.ipynb

Fue el cuaderno inicial de trabajo, contiene las manipulaciones iniciales de los datos del SCR-1, comenzando con los ideales y siguiendo con los obtenidos de la simulación con BS-SOLCTRA.

## Ellipse aprroximation.ipynb

Busca estimar las áreas transversales del plasma formadas por cada camino toroidal en un determinado corte toroidal.

## ParámetrosGeométricos.ipynb

De la experiencia obtenida del procesamiento en Cálculo de errores.ipynb se buscó calcular los perímetros de cada corte toroidal, las áreas transverales nuevamente, los volúmenes del plasma nuevamente y por último el área toroidal.

## Errores Locales, Promedio y Cuadrado Medio.ipynb

Este toma los cálculos y procedimientos con diccionarios realizados en ParámetrosGeométricos.ipynb para adaptarlos a la definición de los diferenciales de área y poder integrar los errores locales sobre la superficie toroidal para estimar el error promedio en el confinamiento. Los errores locales se obtienen definiendo la normal sobre la suerficie en un punto en específico medido desde el eje magnético. Luego, se estima el error cuadrado medio con un procedimiento similar al del error promedio.

Se realizo un importante avance en la estimación de los parámetros geométricos al cargar todos los archivos en un diccionario y operarlos desde ahí.

## Flujo.ipynb

Se estima el flujo magnético a través de una determinada superficie magnética, para confirmar si el flujo se conserva a través de esta.
