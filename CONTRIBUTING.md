# Contribuciones a este repositorio

## Estándar de programación

### Asignación de nombres
    1. **Uso del castellano**: en general se recomienda el uso del lenguaje español castellano para escribir comentarios, nombrar 
    variables, entre otros. Esto para favorecer la claridad y facilidad de lectura del código.
    
    2. **Nombres descriptivos**: cuando se asigna el nombre a una variable (o a una función, clase, método, etc) siempre se debe optar
     por elegir nombres _descriptivos_ que indiquen claramente el propósito de la variable. Por ejemplo, es preferible llamar a una 
     variable para almacenar los resultados de una suma recurrente `suma_Total`  que simplemente llamarle `s`. El uso de nombres 
     largos es perfectamente aceptable en vez de usar de nombres cortos con un significado incierto.

    3. **Variables comienzan en minúscula**: para nombres de variables la primera letra debe estar en minúscula. Si el nombre de la 
    variable es largo, puede usarse mayúsculas al inicio de cada palabra a excepción de la primera. Por ejemplo, una variable para 
    almacenar el número de estados accesibles de un sistema puede llamarse `numeroEstadosAccesibles`.
    
    4. **Iteradores**: los iteradores son las variables sobre las que se itera en un ciclo. Estas variables deben contener _i_,
    _j_ o _k_ como primera letra. Por ejemplo, para iterar sobre las entradas de un vector puede usarse la variable 
    `iEntrada`, o para el caso de una matriz puede usarse `jFila` y `kColumna` para los iteradores.
    5. Abreviaciones: En los nombres de las variables pueden usarse abreviaciones comunes como _max_, _min_ o _temp_. Esto debe 
    cumplir con otras reglas establecidas. Por ejemplo, la variable para almacenar un valor de potencial mínimo puede llamarse 
    `minValorPotencial`, mientras que un valor máximo de energía puede denominarse como `valorEnergiaMax`.
    
    6. **Funciones**: los nombres de las funciones deben comenzar en mayúscula para cada palabra del nombre. Los parámetros de las
    funciones deben seguir las mismas reglas que las variables. Para asignar nombres a funciones, y puesto que las funciones ejecutan 
    acciones, se recomienda el uso de al menos un verbo como parte del nombre. Por ejemplo, una función escrita para ejecutar un 
    camino aleatorio pude denominarse como ``\texttt{EjecutarCaminoAleatorio(nPasos, pProbabilidad)}''.
    
    7. **Clases y métodos**: los nombres de clases y los métodos asociados a ellas deben seguir las reglas establecidas para las funciones.

### Organización del código y formato
    8. **Espacios en blanco e indentación**: se recomienda configurar su editor de texto para que un _TAB_ (presionar la tecla
    TAB) equivalga a cuatro espacios en blanco (como presionar cuatro veces la barra espaciadora). En el caso de 
    python, es parte de su sintaxis el uso de la indentación para identificar instrucciones dentro de un ciclo o condicional.
    Sin embargo, se recomienda usar este mismo estándar aunque no sea necesario, en otros lenguajes (como MATLAB).
    
    9. **Evitar el uso de expresiones complejas**: se recomienda el uso de variables temporales para acortar expresiones matemáticas 
    largas o condiciones complejas. 
    
    10. **Divide y vencerás**: Este enfoque busca crear código modular, es decir, donde se dividen las tareas que deben realizarse entre varias funciones.
    
    11. **Diseño  top-down, escritura botton - up**

    12. **Escritura de datos a archivos para su posterior lectura**:

### Optimización
    13. Cuando se tenga que elegir entre escribir código legible, descriptivo y posiblemente lento, o expresiones menos legibles pero
    optimizadas; es preferible escribir código que se lea fácilmente aunque se sacrifique un poco el rendimiento de este.

### Comentarios y documentación
    14. **Uso de comentarios**: debe hacerse un esfuerzo por escribir código que sea autoexplicativo. Sin embargo, no se descarta que
    de vez en cuando se puedan incluir comentarios que ayuden a comprender mejor el algoritmo. Aunque tampoco se debe abusar de los 
    comentarios para no impedir la fácil lectura del código.
    15. **Uso de _docstrings_**: en python existe un tipo de comentarios que se colocan inmediatamente después de la 
    definición de una función y que sirven como su documentación. Estos son los llamados _docstrings_. Se exhorta su uso en 
    cada función para describir su aplicación. Por ejemplo
    `def DistribucionUniforme(nNumeros = 10000):
    """
    Generar una distribución uniforme de números aleatorios. 
    Esto es un docstring.
    """
    # Generar números aleatorios con una semilla determinada
    np.random.seed(32423)`

### Salidas (de un programa)
    16. Es recomendable incluir muchos `print()` en un programa cuando se está haciendo depuración del código 
    (_debugging_), ya que ayuda a seguir el algoritmo y a comprobar que el programa está haciendo lo que se espera. No 
    obstante, en la versión final de un programa deben eliminarse los `print()` de depuración y dejar solo aquellos que sean 
    estrictamente necesarios.
