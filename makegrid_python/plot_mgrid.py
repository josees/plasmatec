import numpy as np
from matplotlib import colors
import plotly.graph_objects as go 
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt 

class plot_switch(object):

    def __init__(self, data, cutplane):
        self.data = data
        self.extcur = data.extcur
        self.cutplane = cutplane
        
    def plot_type(self, plottype): 
        """
        Dispatch method: 
        he PLOT_MGRID routine plots data read by READ_MGRID.  There are
        various plotting options.
        Options:
            'basic':    Plots the total field on an mgrid plane.
                        'cutplane' option controls which plane (default=1)
            'total':    Plots 3 components and vector plot for each slice in
                        the mgrid file.
            '3dgrid':   Shows the 3D grid Planes.
            'modB':     |B| for a given cutplane
                
        """

        method_name = str(plottype)
        method = getattr(self, method_name, lambda: 'Invalid plot type')
        return method()
    
    def setup_data(self):

        shape = self.data.br.shape
        bx = np.zeros(shape)
        by= np.zeros(shape)
        bz = np.zeros(shape)
        bphi = np.zeros(shape)
        br = np.zeros(shape)

        for i in range(len(self.extcur)):
            bx[i,:,:,:] = np.multiply(self.data.bx[i,:,:,:], self.extcur[i])
            by[i,:,:,:] = np.multiply(self.data.by[i,:,:,:], self.extcur[i])
            bz[i,:,:,:] = np.multiply(self.data.bz[i,:,:,:], self.extcur[i])
            bphi[i,:,:,:] = np.multiply(self.data.bphi[i,:,:,:], self.extcur[i])
            br[i,:,:,:] = np.multiply(self.data.br[i,:,:,:], self.extcur[i])
        
        return np.array([bx, by, bz, bphi, br])

    def setup_data_2d(self):
        """
        for plots: 
            - 'basic' 
            - 'total'
            - 'modB'

        """

        bx, by, bz, bphi, br = self.setup_data()
        
        bxt = np.sum(bx,axis = 0)
        byt = np.sum(by,axis = 0)
        bzt = np.sum(bz,axis = 0)
        brt = np.sum(br,axis = 0)
        bphit = np.sum(bphi,axis = 0)

        return np.array([bxt, byt, bzt, brt, bphit])

    def basic(self):
        
        fig, axs = plt.subplots(2,2, figsize=(10,9))

        _,_, bzt, brt, bphit = self.setup_data_2d()

        raxis2d, zaxis2d = np.meshgrid(self.data.raxis, self.data.zaxis)
        print(zaxis2d.shape)

        br2 = np.multiply(brt,brt)
        bz2 = np.multiply(bzt,bzt)
        bphi2 = np.multiply(bphit,bphit)

        bmag = np.sqrt(br2+bz2+bphi2)
        c1 = axs[0,0].scatter(raxis2d, zaxis2d,c=bphit[self.cutplane,:,:], cmap='jet')
        axs[0,0].set_xlabel('R-Axis')    
        axs[0,0].set_ylabel('Z-Axis')
        axs[0,0].set_title('Toroidal Field')

        c2 = axs[0,1].scatter(raxis2d, zaxis2d,c=brt[self.cutplane,:,:], cmap='jet')
        axs[0,1].set_xlabel('R-Axis')    
        axs[0,1].set_ylabel('Z-Axis')
        axs[0,1].set_title('Radial Field')
        
        c3 = axs[1,0].scatter(raxis2d, zaxis2d,c=bzt[self.cutplane,:,:], cmap='jet')
        axs[1,0].set_xlabel('R-Axis')    
        axs[1,0].set_ylabel('Z-Axis')
        axs[1,0].set_title('Vertical Field')

        c4 = axs[1,1].scatter(raxis2d, zaxis2d,c=bmag[self.cutplane,:,:], cmap='jet')
        axs[1,1].set_xlabel('R-Axis')    
        axs[1,1].set_ylabel('Z-Axis')
        axs[1,1].set_title('Vertical Field')

        fig.colorbar(c1, ax=axs[0,0], orientation='vertical', fraction=.1)
        fig.colorbar(c2, ax=axs[0,1], orientation='vertical', fraction=.1)
        fig.colorbar(c3, ax=axs[1,0], orientation='vertical', fraction=.1)
        fig.colorbar(c4, ax=axs[1,1], orientation='vertical', fraction=.1)


        fig.show()  
        fig.savefig('out.png', dpi=150)

    def Grid3d(self):

        bxt, byt, bzt, brt, bphit, bmin, bmax = self.setup_data_2d()

        raxis2d, zaxis2d = np.meshgrid(self.data.raxis, self.data.zaxis)
        
        bmag = np.sqrt(bxt**2+byt**2+bzt**2)
        layout = go.Layout(
             scene=dict(
                 aspectmode='data'
        ))

        phi = self.data.phi

        xaxis = np.einsum('ij, k', raxis2d, np.cos(2*phi), optimize = True)
        yaxis = np.einsum('ij, k', raxis2d, np.sin(2*phi), optimize = True)

        fig = go.Figure(layout= layout)
        ind = np.arange(0,self.data.dims['phi'], 4)

        for i in ind:

            fig.add_trace(go.Surface(x = xaxis[:,:,i],
                                    y = yaxis[:,:,i], 
                                    z = zaxis2d[:,:],
                                    surfacecolor = bmag[:,:,i]))

        fig.show()
        


def plot_mgrid(data, cutplane, plottype): 
    
    a = plot_switch(data,cutplane)
    a.plot_type(plottype)


# CLASS NOT FINISHED --- FINISHING UNTIL RIGHT INPUT DATA IS AVAILABL
