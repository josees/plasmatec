import numpy as np


def read_coils(filename):
    periods = 0         
    with open(filename, mode='r') as file:
        
        fileread = file.readlines()
        coils = {}
        coil = []
        count = 0
        for iLine in range(len(fileread)):
            
            if iLine == 0: 
                periods = int(fileread[iLine][-2])
                coils['periods'] = periods
                continue
            elif iLine == 1 or iLine == 2:
                continue

            else:
                line = fileread[iLine].split('  ')

                if len(line) == 5:
                    count += 1
                    coil.append([float(elem) for elem in line[0:4]])
                    coils['coil'+str(count)+'_'+line[4][0:-1]] = coil
                    coil = []

                elif line[0] == 'end\n' or line[0] =='END\n':
                    break
 
                else: 
                    coil.append([float(elem) for elem in line])

    for key in coils.keys():
        coils[key] = np.array(coils[key]).T

    return coils
                

      
                
        


