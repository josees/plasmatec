# Folder que contiene los archivos para la lectura de archivos .nc dados por el módulo VMEC - MAKEGRID. 

## Lectura de datos: read_mgrid.py 

Uso: data = read_mgrid(archivo)
> Colaborador: Johansell Villalobos

## Graficación de datos: plot_mgrid.py

Uso = plot_mgrid(data, extcur, cutplane, ''modo de graficación'') #modo de graficación especificado en el archivo.
> Colaborador: Johansell Villalobos

## Cálculo de campo a partir de archivo de bobinas: coil_biot.py
> Colaborador: Johansell Villalobos

## Estimación del Flujo Magnético y la superficie transversal de MEDUSACR

Cuaderno de desarrollo para el cálculo inicial del flujo magnético de MEDUSACR, tomando las lecturas de los datos realizados con los códigos de Johansell, por medio de una integración de Monte Carlo, estimación del área transversal por medio de Monte Carlo y una regresión lineal múltiplo para la extrapolación del campo Magnético toroidal.
> Colaboradores: Allan González y Bryam Núñez.

## Flujo Magnético de MEDUSACR

Archivo final de la estimación del flujo magnético toroidal de MEDUSACR a partir de la parametrización que se realizó en el cuaderno anterior, incluye también la estimación del área por método de Monte Carlo.

> Colaboradores: Allan González y Bryam Núñez.
