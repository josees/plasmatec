import numpy as np


def coil_biot_prep(coildata):

    data = []
    cur = []
    for key in coildata.keys():
        if key != 'periods': 
            x, y, z, cur = coildata[key]
            data.append([x, y, z])
            cur.append(cur)

    data = np.array(data)
    cur = np.array(cur)
    L = np.sqrt(np.sum((np.diff(data))**2, axis=0))
    unit_vectors = (np.diff(data)/L).T



        



