# Procesamiento de los datos obtenidos por la Sonda Langmuir

De momento los cuadernos de Jupyter están orientados a resolver para datos obtenidos por una configuración de sonda simple.

## LangmuirProbeBasics.ipynb

Está orientado directamente a la obtención de las estimaciones utilizando las recomendaciones de Chen en Principles of Plasma Processing.

## Calibración - Siguiendo el artículo de Chen.ipynb

Busca aplicar las distintas teorías para las dos corrientes para realizar el análisis de calibración que realiza Chen en Calibration of Langmuir probes against microwaves and plasma oscillation probes, para poder seleccionar el modelo que mejor se ajusta a las distintas regiones del plasma del SCR-1.

## Literatura

Es una carpeta que contiene las referencias sobre las que se basan los análisis de los Jupyter Notebooks mencionados anteriormente.
