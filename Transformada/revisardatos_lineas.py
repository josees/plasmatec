"""
Este código itera a través de un directorio que contiene las trayectorias
de las partículas calculadas y revisa cada archivo de texto, esto debido a que las columnas de datos
no están completas.


Autor: Johansell Villalobos
Ingeniería Física, TEC

Última edición: 09/09/2020
"""

import os
import numpy as np
import csv



def revisar_datos(nombre,n):
	"""
	Entradas: nombre--string, nombre del archivo a revisar
	Salidas: nombre -- string, positions--posiciones de errores en el archivo (lineas)
	Descrip: revisa el archivo de texto para que sea de la forma (n,4)
	"""
	#Para este metodo es mejor abrir los archivos con csv.reader debido a que si se utiliza np.loadtxt, a menos de que sea un archivo sin errores en columnas, no va a abrir el archivo

	part_path = []

	with open(nombre ,newline='') as path:
		part = csv.reader(path, delimiter='\t')
		for line in part:
			part_path.append(line)
		path.close()

	positions = []
	for pos in range(len(part_path)):
		if len(part_path[pos]) != n:
			positions.append(pos)

	return nombre, positions, len(part_path)

def revisar_directorio(directory_path,n):
	"""
	Entradas: directory_path--string, nombre del archivo a revisar
	Salidas: errores--list(), devuelve una lista con el siguiente formato:

			[str(PATH del archivo) , list(posiciones de errores en el archivo) , int(cantidad de lineas) , str(tamaño del archivo)]

	Descrip: revisa el archivo de texto para que sea de la forma (m,n)
	"""
	errores = []
	for filename in os.listdir(directory_path):
		flname = str(directory_path)+'/'+str(filename)
		nom , pos, lenfile = revisar_datos(flname,n)
		fileSize = os.path.getsize(flname)
		errores.append([nom,pos,lenfile,str(round(fileSize/(1024*1024),3))+'MB'])

	return np.array(errores)
