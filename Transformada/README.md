# Archivos transformada rotacional

## rotational transform.py:
script con la implementación del método para el cálculo de la transformada rotacional tomado del artículo [Calculating Rotational Transform Following Field Lines](http://jasosx.ils.uec.ac.jp/JSPF/JSPF_TEXT/jspf2003/jspf2003_04/jspf2003_04_321.pdf)

## Ideas para arreglar la integración numérica
* Buscar puntos que estén igualmente espaciados (angularmente) para ejecutar el RK4 sobre ellos y comparar con la implementación y los puntos que tengo actualmente.
* Usar otro método para validar el RK4 que se usa en este momento. Predictor corrector en una alternativa.
* Buscar un método que use un espaciamiento (step) no uniforme. Es decir, que se pueda usar con los puntos que tengo en este momento.
* Hacer la EDO [14] adimensional.
* Dado a que no contamos con la distancia angular total y  los espaciamientos (steps necesarios para el método numérico)
  ya están predefinidos por la simulación de las partículas confinadas debemos implementar RK4 con espaciamientos no uniformes
  dados por la diferencia angular entre dos puntos (coordenadas esféricas).
* Es necesario corregir el método numérico ya que debe implementar la recursividad, dada por los valores anteriores.
* Recordar que la variable independiente es fi y la variable dependiente es w

## Tareas pendientes: (31-10-2019)
* Pasar el código a un jupyter notebook
