# PlasmaTEC

Códigos del Laboratorio de Plasmas TEC

## Transformada

Contiene los scripts  que se han desarrollado para calcular la transformada rotacional con los datos de BS-SOLCTRA.

### Colaboradores:

* Esteban Pérez
* Johansell Villalobos

## Sonda-de-Langmuir

Archivos relacionados la estimación de parámetros característicos del plasma a partir de los datos obtenidos con la sonda de Langmuir.

### Colaboradores

* Ricardo Solano
* Allan González

## Interferómetro

Archivos relacionados con la adquisición de datos, la simulación de la onda pasando por el plasma y el procesamiento de los datos del Interferómetro Heterodino de Microondas del SCR-1.

### Colaboradores

* Miguel Rojas
* Allan González
* María Fernanda Quesada

## Cálculos de errores

Cuadernos de Jupyter orientados al cálculo de errores en el confinamiento, incluyen cálculos de volumen del plasma, superficies transversales y toroidales encerrados por cada path toroidal del campo magnético calculado con BS-SOLCTRA.

### Colaboradores

* Ricardo Solano
* Bryam Núñez
* Allan González

## Diagnósticos magnéticos

Colaboradores: Arnoldo

## Diwo

Archivos relacionados con el código para controlar el SCR-1 desde el PXIe y su documentación.

### Colaboradores:

* Farid Marín
* Roger
* Agustín Delgado

## makegrid_python

Contiene los códigos para realizar la lectura de los archivos *.nc que son la salida de VMEC. Además contiene dos cuadernos que utilizan estos datos para la estimación del flujo magnético toroidal y la parametrización del campo magnético toroidal.

### Colaboradores

* Allan González
* Bryam Núñez
* Johansell Villalobos

## VMEC MEDUSA

Contiene la información y códigos relacionados a la implementación de VMEC para MEDUSA CR

### Colaboradores

* Johansell Villalobos
* Ricardo Solano
* Bryam Núñez 

